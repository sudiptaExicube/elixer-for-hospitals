import React, { useEffect } from 'react';
import { Provider } from 'react-redux';
import { ThemeProvider } from './src/constants/ThemeContext';
import Navigator from './src/navigation/Navigator';
import { store } from './src/Store/AppStore';
import { StatusBar } from 'react-native';
import { LogBox } from 'react-native';

function App() {
  
  useEffect(() => {
      LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
      // console.disableYellowBox = true;
      console.disableYellowBox = ['Warning: Each', 'Warning: Failed'];
      console.disableYellowBox = true;

  }, [])

  return (
  <Provider store={store}>
    <ThemeProvider>
    <StatusBar
       backgroundColor={"#FFF"}
       
       barStyle="dark-content"
       />
      <Navigator />
    </ThemeProvider>
  </Provider>

  );
}

export default App;
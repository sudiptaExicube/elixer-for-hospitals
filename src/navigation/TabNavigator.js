import React, { useEffect, useRef } from 'react';

//Bottom Navigation import
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
const Tab = createBottomTabNavigator();
import Icon from 'react-native-vector-icons/Ionicons';
import * as Animatable from 'react-native-animatable';
import { StyleSheet, Text, TouchableOpacity, View,Image, Platform } from 'react-native';
import {HomeScreen, MenuScreen } from '../screens';
import { Theme } from '../constants';
import ScheduleScreen from '../screens/TabScreens/Schedule/ScheduleScreen';
import EarningScreen from '../screens/TabScreens/Earning/EarningScreen';
import StaffScreen from '../screens/TabScreens/Staffing/StaffScreen';

const TabArr = [
  { route: 'Home', label: 'Home', activeIcon: require('../assets/images/homeActive.png'), inActiveIcon: require('../assets/images/homeinActive.png'), component: HomeScreen },
  { route: 'Schedule', label: 'Schedule', activeIcon: require('../assets/images/calenderActive.png'), inActiveIcon: require('../assets/images/calenderinActive.png'), component: ScheduleScreen },
  // { route: 'Search', label: 'Staffing', activeIcon: require('../assets/images/searchActive.png'), inActiveIcon: require('../assets/images/searchinActive.png'), component: SearchScreen },
  { route: 'Staff', label: 'Staffing', activeIcon: require('../assets/images/staffActive.png'), inActiveIcon: require('../assets/images/staffInActive.png'), component: StaffScreen },
  { route: 'Earning', label: 'Payment', activeIcon: require('../assets/images/earningActive.png'), inActiveIcon: require('../assets/images/earninginActive.png'), component: EarningScreen },
  { route: 'Menu', label: 'Menu', activeIcon: require('../assets/images/menuActive.png'), inActiveIcon: require('../assets/images/menuinActive.png'), component: MenuScreen }
];

// const IOSStyle = {
//   elevation:0.8,
//   backgroundColor:Theme.colors.light_primary
// }
// const AndStyle = {
//   height: 60,
//   elevation:0.8,
//   backgroundColor:Theme.colors.light_primary
// }

const TabButton = (props) => {
  const { item, onPress, accessibilityState } = props;
  const focused = accessibilityState.selected;
  const viewRef = useRef(null);



  useEffect(() => {
    // if (focused) {
    //   viewRef.current.animate({0: {scale: 0.5, rotate: '0deg'}, 1: {scale: 1.5, rotate: '360deg'}});
    // } else {
    //   viewRef.current.animate({0: {scale: 1.5, rotate: '360deg'}, 1: {scale: 0.9, rotate: '0deg'}});
    // }
  }, [focused])



  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={1}
      style={styles.container}>
      <View style={{flex:1,flexDirection:'column',justifyContent:'space-evenly'}}>
        {/* <View style={{flex:0.5,flexDirection:'row',justifyContent:'center'}}>
          <View style={{height:5}}>
            {focused?
              <View style={{height:5,width:25,backgroundColor:Theme.colors.primary,borderRadius:2}}></View>
            :null
            }
          </View>

        </View> */}
        <View style={{flex:4}}>
          <Animatable.View
            ref={viewRef}
            duration={1000}
            style={styles.animatable_container}>
            {/* <Icon size={16} name={focused ? item.activeIcon : item.inActiveIcon} color={focused ? Theme.colors.primary : Theme.colors.grey} /> */}
            <Image
              style={{height:20,width:20,marginTop:5}}
              source={focused ? item.activeIcon : item.inActiveIcon}
            />
          </Animatable.View>
        </View>
        {/* flex:1.7, */}
        <View style={{justifyContent:'flex-start',marginBottom:5}}>
            <Text style={{fontSize:12,letterSpacing:0.5,textAlign:'center',fontFamily: Theme.FontFamily.normal,color:focused ? Theme.colors.primary : Theme.colors.grey}}>{item?.label}</Text>

        </View>


      </View>
    </TouchableOpacity>
  )
}


export default function TabNavigator() {

  // onClick(()=>{
  //   console.log("working");
  // })

  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        headerShadowVisible:false,
        tabBarStyle: {
          // height: 60,
          // height:Platform.OS == 'ios'? IOSStyle : AndStyle,
          // position: 'absolute',
          elevation:0.8,
          // borderTopLeftRadius:7,borderTopRightRadius:7,
          // backgroundColor:Theme.colors.light_primary.Animatable
          backgroundColor:Theme.colors.white,borderRadius:10,
          borderTopWidth:1
        }
      }}
    >
      {TabArr.map((item, index) => {
        return (
          <Tab.Screen key={index} name={item.route} component={item.component}
            options={{
              tabBarShowLabel: false,
              
              tabBarButton: (props) => <TabButton {...props} item={item} 
              // onPress={() => {
              //   // navigation.dispatch(DrawerActions.openDrawer())
              //   console.log('working...');
              // }}
              />
            }}
          />
        )
      })}
      {/* <Tab.Screen name="Sidemenu" 
        onPress={() => {
          // navigation.dispatch(DrawerActions.openDrawer())
          console.log('working...');
          // props.navigation.navigate(MenuScreen)
        }}
      component={MenuScreen} 
      /> */}
    </Tab.Navigator>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  animatable_container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
})

import { Dimensions } from "react-native";

export const windowHeight=Dimensions.get('window').height
export const windowWidth=Dimensions.get('window').width
export const customHeader_height=60
export const global_iconSize=24
export const customBody_height=windowHeight-95


const WindowData = { windowHeight,windowWidth,customHeader_height,global_iconSize,customBody_height };

export default WindowData;
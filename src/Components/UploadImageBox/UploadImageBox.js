import { View, Text, TouchableOpacity } from 'react-native'
import React from 'react'

import CameraIcon from '../../assets/images/cameraIcon.svg'
import { Theme } from '../../constants'

const UploadImageBox = (props) => {
  return (
  //   <View>
  //   <View style={[{backgroundColor: '#DADADA',
  //   borderRadius: 8,alignItems:'center',justifyContent:'center',
  //   aspectRatio:1,
  //   // width:120,
  //   height:120},props.style]}>
  //     <CameraIcon style={{alignSelf:'center'}} height={55} width={55}/>
  //   </View>
  //   <Text style={{
  //   fontFamily: Theme.FontFamily.normal,
  //   fontStyle: 'normal',
  //   fontWeight: '500',
  //   fontSize: 10,
  //   marginTop:'3%',
  //   alignItems: 'center',
  //   textAlign: 'center',
  //   letterSpacing: -0.333333,
  //   textTransform: 'capitalize',
  //   color: '#000000',
  //   flexWrap:'wrap',width:'100%'
  // }}>{props.name ? props.name : 'image size max 15MB'}</Text>
  //   </View>
    <View>
        <TouchableOpacity 
          onPress={props.imageClick}
          style={[{aspectRatio:1,height:120}]}>
            <View 
                style={[{backgroundColor: '#DADADA',
                borderRadius: 8,alignItems:'center',justifyContent:'center',
                aspectRatio:1,
                height:120},props.style]}>
                  <CameraIcon style={{alignSelf:'center'}} height={55} width={55}/>
            </View>
            <View>
               <Text style={{
               fontFamily: Theme.FontFamily.normal,
               fontStyle: 'normal',
               fontWeight: '500',
               fontSize: 10,
               marginTop:'3%',
               alignItems: 'center',
               textAlign: 'center',
               letterSpacing: -0.333333,
               textTransform: 'capitalize',
               color: '#000000'
             }}>{props.lebel ? props.lebel : 'image size max 15MB'}</Text>
            </View>

        </TouchableOpacity>
    </View>
  )
}

export default UploadImageBox

import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import { GlobalStyles, WindowData } from '../../constants';
// create a component
const Styles = () => {
  // const {colorTheme} = useTheme();
  return StyleSheet.create({
    mainContainer:{
      flex:1,
      backgroundColor:'#fff'
    },
    container: {
      flex:1,
      width:WindowData.windowWidth,
    },
    childrenContainer: {
        width: '100%',
        // paddingLeft:15,
        // paddingRight:15
    }
  });
};
export default Styles;


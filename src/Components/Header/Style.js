//import liraries
import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import { WindowData } from '../../constants';
import { customHeader_height } from '../../constants/window';
// create a component
const Styles = () => {
  // const {colorTheme} = useTheme();
  return StyleSheet.create({
    container: {
      height:customHeader_height,
      width:WindowData.windowWidth
    },
  });
};
export default Styles;

//import liraries
import { useNavigation } from '@react-navigation/native';
import React, { Component, useEffect, useRef } from 'react';
import { View, Text, Image, TouchableOpacity, Alert, TouchableWithoutFeedback } from 'react-native';
import * as Animatable from 'react-native-animatable';
import Icon from 'react-native-vector-icons/Ionicons';
import { Theme, WindowData } from '../../constants';
import Styles from './Style';

// create a component
const Header = ({ backButton_animationRef, isShownHeaderLogo, headerTitle, showBackButton, backButtonClick,

  icon1Name,icon1_animationRef,icon1Color,
  svgIconPath,
  icon3Name,icon3_animationRef, icon2Name,icon2_animationRef,
  icon1Click, icon2Click, icon3Click,
  backIconName,headerBackground,headerStyle,headerTextStyle
}) => {
  const navigation = useNavigation();
  const styles = Styles();
  const {colors,FontFamily,sizes} = Theme


  return (
    <View style={[styles.container,headerStyle,{backgroundColor : headerBackground ? headerBackground : ''}]}>
      <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', paddingLeft: 15, paddingRight: 15 }}>
        <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
          {isShownHeaderLogo ?
            <View>
              {/* <Image style={{ height: 50, width: 120}} resizeMode={'contain'} */}
              <Image style={{ height: 50, width: 50}} resizeMode={'contain'}
                source={require('../../assets/images/logo.png')}
              />
            </View>
            :
            <View style={{ flexDirection: 'row' }}>
              {showBackButton ?
                <Animatable.View ref={backButton_animationRef} duration={1000} style={{flexDirection:'column',justifyContent:'center'}}>
                  <TouchableWithoutFeedback onPress={ backButtonClick } >
                    <Icon size={WindowData.global_iconSize} name={backIconName ? backIconName : 'arrow-back-outline'} color={colors.black} style={headerTextStyle ?headerTextStyle :null } />
                  </TouchableWithoutFeedback>
                </Animatable.View>
                : null
              }

              <View style={{ justifyContent: 'center', paddingLeft: 3 }}>
                <Text style={[{fontSize:sizes.h5,fontFamily:FontFamily.bold,color:Theme.colors.black},headerTextStyle?headerTextStyle:null]}>{headerTitle}</Text>
              </View>
            </View>

          }
        </View>
        
        <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
          <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
            {icon1Name ?
              <Animatable.View
                ref={icon1_animationRef} duration={1000} style={{ paddingLeft: 20 }}>
                <TouchableWithoutFeedback onPress={icon1Click} >
                  <Icon size={WindowData.global_iconSize} name={icon1Name} color={icon1Color ? icon1Color : colors.black} />
                </TouchableWithoutFeedback>
              </Animatable.View>
              : null}
              {/* For SVG icon */}
              {svgIconPath ?
              <Animatable.View
                ref={icon1_animationRef} duration={1000} style={{ paddingLeft: 20 }}>
                <TouchableWithoutFeedback onPress={icon1Click} >
                  {/* <Icon size={WindowData.global_iconSize} name={icon1Name} color={icon1Color ? icon1Color : colors.black} /> */}
                  {svgIconPath}
                </TouchableWithoutFeedback>
              </Animatable.View>
              : null}

            {/* {icon2Name ?
              <Animatable.View
                ref={icon2_animationRef} duration={1000} style={{ paddingLeft: 20 }}>
                <TouchableWithoutFeedback onPress={icon2Click} >
                  <Icon size={WindowData.global_iconSize} name={icon2Name} color={colors.black} />
                </TouchableWithoutFeedback>
              </Animatable.View>
              : null} */}

            {/* {icon3Name ?
              <Animatable.View
                ref={icon3_animationRef} duration={1000} style={{ paddingLeft: 20 }}>
                <TouchableWithoutFeedback onPress={icon3Click} >
                  <Icon size={WindowData.global_iconSize} name={icon3Name} color={colors.black} />
                </TouchableWithoutFeedback>
              </Animatable.View>
              : null} */}
          </View>
        </View>
      </View>
    </View>
  );
};

//make this component available to the app
export default Header;


// import {View, Text, Pressable} from 'react-native';
// import React from 'react';
// import {Picker} from '@react-native-picker/picker';
// import Style from './Style';
// import { Theme } from '../../constants';
// import DropDownArrow from '../../assets/images/DropDownArrow.svg'

// const Styles = Style();
// const DropdownPicker = ({selectedValue,style,setDocumentBottomVisible}) => {
//   return (

//       <Pressable style={[Styles.inputBoxStyle,style]} onPress={()=>{setDocumentBottomVisible(true)}}>
//         <Text style={Styles.text} >{selectedValue}</Text>
//         <DropDownArrow style={{end:0,position:'absolute',marginEnd:10,size:20}}/>
//       </Pressable>
    
//   );
// };

// export default DropdownPicker;

import {View, Text, Pressable} from 'react-native';
import React from 'react';
import {Picker} from '@react-native-picker/picker';
import Style from './Style';
import { Theme } from '../../constants';
import DropDownArrow from '../../assets/images/DropDownArrow.svg'

const Styles = Style();
const DropdownPicker = ({selectedValue,style,setDocumentBottomVisible,isHaveTextField,textFieldValue,textStyle}) => {
  return (

      <Pressable style={[Styles.inputBoxStyle,style]} 
      // onPress={()=>{
      //   setDocumentBottomVisible(true)
      //   }}
      onPress={setDocumentBottomVisible}
      >
        {isHaveTextField?<Text style={{backgroundColor:Theme.colors.white,position:'absolute',top:-11,marginStart:14,fontFamily:Theme.FontFamily.normal,color:Theme.colors.black,fontWeight:'500'}}>{textFieldValue}</Text>:null}
        <Text style={[Styles.text,textStyle ]}>{selectedValue}</Text>
        <DropDownArrow style={{end:0,position:'absolute',marginEnd:10,size:20}}/>
      </Pressable>
    
  );
};

export default DropdownPicker;


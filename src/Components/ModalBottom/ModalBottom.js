//import liraries
import React, { Children, Component, useEffect, useRef, useState } from 'react';
import {
    SafeAreaView,
    View,
    StyleSheet,
    ScrollView,
    Text,
    Image,
    TextInput,
    TouchableOpacity,
    Pressable,
    Animated,
    Modal
} from 'react-native';
import { Theme, WindowData } from '../../constants';
import RedCross from '../../assets/images/RedCross.svg';


// create a component
const ModalBottom = ({
    children,
    modalVisible,
    modalHeaderText,
    modalHeight,
    modalCloseAction
}) => {
    
    return (
        <View>
            <Modal
                style={styles.modalView}
                animationType="slide"
                transparent={true}
                visible={modalVisible}
            >
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center',backgroundColor:'rgba(0,0,0,0)'}}>
                    <View style={styles.containerStyle}>
                        <View style={[styles.content,{height:modalHeight?modalHeight:WindowData.windowHeight/2}]}>
                            <View style={[styles.headerMain]}>
                                <Text style={[styles.headerText]}>{modalHeaderText}</Text>
                                <Pressable onPress={modalCloseAction}>
                                    {/* <Image source={AllSourcePath.LOCAL_IMAGES.cross} style={styles.backIcon} /> */}
                                    <RedCross style={styles.backIcon} />
                                </Pressable>
                            </View>
                            <View style={styles.childContainer}>
                                {children}
                            </View>
                        </View>
                    </View>

                </View>
            </Modal>

        </View>
    );
};

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'flex-end',
        

    },
    content: {
        width: '100%',
        height: '60%',
        paddingBottom: 16,
        // backgroundColor: Theme.colors.white,
        overflow: 'hidden',
        backgroundColor:'#f3f3f3',
        borderTopStartRadius:20,
        borderTopEndRadius:20,
        //backgroundColor: 'red'
        borderWidth:1,
        // borderTopColor:'red'
        borderColor:'white'

    },
    headerMain: {
        borderBottomColor: Theme.colors.white,
        borderBottomWidth: 3,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        //marginLeft: 12,
        paddingVertical: 18,
        paddingHorizontal: 16
    },
    backIcon: {
        height: 22,
        width: 24,
        resizeMode: 'contain',
        marginLeft: 12,
    },
    headerText: {
        //marginLeft: 12,
        fontSize: 15,
        fontFamily: Theme.FontFamily.medium,
        color: Theme.colors.black
    },
    childContainer:{ paddingHorizontal: 12, marginBottom:30}
})
//make this component available to the app
export default ModalBottom;
import { View, Text, TextInput } from 'react-native'
import React from 'react'
import Style from './Style'
import { Theme } from '../../constants'

const Styles=Style()

const EditTextInputBox = (props) => {
  return (
    <View
          style={[Styles.inputBoxStyle,props.style]}>
          {props.icon}
          <TextInput
            style={{flex: 1,paddingStart:15,color:Theme.colors.black,fontSize:12,fontFamily:Theme.FontFamily.normal}}
            onChangeText={props.onChangeText}
            value={props.text}
            placeholder={props.placeHolderText}
            placeholderTextColor="#000"
            underlineColorAndroid="transparent"
            keyboardType={props.keyboardType ? props.keyboardType : 'default'}
            maxLength={props.maxLength ? props.maxLength : null}
            secureTextEntry={props.secureTextEntry}
            autoCorrect={false}
            spellCheck={false}
            editable = {props.editable ? props.editable : true}
          />
          {props.rightIcon}

        </View>
  )
}

export default EditTextInputBox
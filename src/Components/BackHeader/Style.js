import { View, Text, StyleSheet } from 'react-native'
import React from 'react'
import { Theme } from '../../constants';

const Style = () => {

    return StyleSheet.create({
      backHeaderText:{
        fontFamily: Theme.FontFamily.normal,
        fontStyle: 'normal',
        fontWeight: '600',
        fontSize: 19,
        lineHeight: 23,
        textAlign: 'center',
        letterSpacing: -0.333333,
        color: Theme.colors.black,
        marginStart:'4%'
      }
      });
}

export default Style
import { View, Text, TextInput, Keyboard, TouchableOpacity } from 'react-native'
import React, { useState } from 'react'
import Style from './Style'
import DatePicker from 'react-native-date-picker'
import { Theme } from '../../constants'
import EvilIcon from 'react-native-vector-icons/EvilIcons'

const Styles=Style()

const getProperDate=(date)=>{
    return `${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`
}

const MyDatePicker = (props) => {
  //const [thedate, setDate] = useState(new Date())
  
  const [open, setOpen] = useState(false)
  return (
    <TouchableOpacity onPress={() => setOpen(true)}
          style={[Styles.inputBoxStyle,props.style]}>
          
          <Text style={{flex: 1,paddingStart:15,color:Theme.colors.black}}>{props.firstTime!=null?props.firstTime:getProperDate(props.date)}</Text>
          <EvilIcon name="calendar" size={25} color={Theme.colors.black} style={{paddingRight:7}}/>
          <DatePicker
        modal
        open={open}
        date={props.date}
        onConfirm={(date) => {
          setOpen(false)
          props.setDate(date)
          props.setFirstTime(null)
        }}
        onCancel={() => {
          setOpen(false)
        }}
        mode="date"
      />
        </TouchableOpacity>
  )
}

export default MyDatePicker
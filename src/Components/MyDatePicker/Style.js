import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {useTheme} from '../../constants/ThemeContext'


// create a component
const Style = () => {
  const {colorTheme} = useTheme;
  return StyleSheet.create({
    inputBoxStyle: {
        borderWidth: 1,
        borderColor: '#BFA058',
        borderRadius: 10,
        width: '87%',
        height: '7%',
        alignSelf: 'center',
        flexDirection: 'row',
        alignItems: 'center',
      }
  })
}

export default Style;

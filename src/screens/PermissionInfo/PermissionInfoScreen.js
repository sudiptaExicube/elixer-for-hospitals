import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	ScrollView, Image
} from 'react-native';
import React, { useRef, useState } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../constants';

import Style from './Style';
import { BlackButton, EditTextInputBox, ScreenLayout } from '../../Components';
import CancelIcon from '../../assets/images/cancelled_icon.svg';
import KeyIcon from '../../assets/images/Key.svg'
import * as Animatable from 'react-native-animatable';

const PermissionInfoScreen = props => {
	const Styles = Style()
	const globalstyles = GlobalStyles();
	const onPress = () => HelperFunctions.sampleFunction('its working fine');

	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */
	const hospitalIconImage = require('../../assets/images/hospital_icon.png');

	const [selectedIndex, setSelectedIndex] = useState(null);
	const [acceptTerms, setAcceptTerms] = useState(false);
	const activeTermsStyle = {
		outerStyle:{backgroundColor:Theme.colors.white},
		iconStyle:{color:Theme.colors.primary}
	}
	const defaultTermsStyle = {
		outerStyle:{backgroundColor: Theme.colors.background_shade1},
		iconStyle:{color:Theme.colors.grey}
	}

	const teamDetails = [
		{ value: '8am to 12pm' },
		{ value: '12pm to 4pm' },
		{ value: '4pm to 8pm' },
		{ value: '8pm to 12am' },
		{ value: '12am to 4am' },
		{ value: '4am to 8am' }
	]

	return (


		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={''}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={'#F8FBFF'}
			// headerBackground={'red'}
			showHeaderLine={false}
			// customBackground={Theme.colors.white}
			customBackground={'#F8FBFF'}
			// customBackground={'red'}
			isBackBtn={true}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				props.navigation.goBack();

			}}
			backButton_animationRef={viewRef}
			showScrollView={false}
		>
			<View style={Styles.mainView}>
				<View style={{ alignSelf: 'center' }}>
					{/* <Image style={{ height: 100, width: 100}} resizeMode={'contain'}
                source={require('../../assets/images/logo.png')}
              /> */}
					<Animatable.Image
						style={{ height: 80, width: 80 }}
						source={require('../../assets/images/logo.png')}
						// animation={'zoomInUp'}
						animation={'pulse'}
						easing="ease-out"
						iterationCount="infinite"
					>
					</Animatable.Image>
				</View>
				<View style={{ paddingHorizontal: 20 }}>
					<View style={{ paddingTop: 30 }}>
						<Text
							style={{ fontFamily: Theme.FontFamily.medium, color: Theme.colors.black, fontSize: Theme.sizes.h5, paddingBottom: 5 }}
						>
							You’re all set to go!
						</Text>
						<Text
							style={{ fontFamily: Theme.FontFamily.medium, color: Theme.colors.grey, fontSize: Theme.sizes.sm }}
						>
							Before starting please ensure you’ve completed all data correctly. Check below.
						</Text>

					</View>


					<View style={{ paddingTop: 30 }}>
						<View style={{ flexDirection: 'row', marginBottom: 20,paddingTop:20 }}>
							<View>
								<Text
									style={{ fontFamily: Theme.FontFamily.bold, color: Theme.colors.black, fontSize: Theme.sizes.h6 }}
								>
									01.
								</Text>
							</View>
							<View style={{ paddingLeft: 5 }}>
								<Text
									style={{ fontFamily: Theme.FontFamily.bold, color: Theme.colors.black, fontSize: Theme.sizes.h6, paddingBottom: 5 }}
								>
									Personal Information
								</Text>
								<Text
									style={{ fontFamily: Theme.FontFamily.normal, color: Theme.colors.primary, fontSize: Theme.sizes.tiny }}
								>
									Check and update all your basic information like name , email, etc.
								</Text>
							</View>
						</View>

						<View style={{ flexDirection: 'row', marginBottom: 20,paddingTop:20 }}>
							<View>
								<Text
									style={{ fontFamily: Theme.FontFamily.bold, color: Theme.colors.black, fontSize: Theme.sizes.h6 }}
								>
									02.
								</Text>
							</View>
							<View style={{ paddingLeft: 5 }}>
								<Text
									style={{ fontFamily: Theme.FontFamily.bold, color: Theme.colors.black, fontSize: Theme.sizes.h6, paddingBottom: 5 }}
								>
									Identity Proofs 
								</Text>
								<Text
									style={{ fontFamily: Theme.FontFamily.normal, color: Theme.colors.primary, fontSize: Theme.sizes.tiny }}
								>
									Verified your Identity with valid documents.
								</Text>
							</View>
						</View>

						<View style={{ flexDirection: 'row', marginBottom: 20,paddingTop:20 }}>
							<View>
								<Text
									style={{ fontFamily: Theme.FontFamily.bold, color: Theme.colors.black, fontSize: Theme.sizes.h6 }}
								>
									03.
								</Text>
							</View>
							<View style={{ paddingLeft: 5 }}>
								<Text
									style={{ fontFamily: Theme.FontFamily.bold, color: Theme.colors.black, fontSize: Theme.sizes.h6, paddingBottom: 5 }}
								>
									Professional Information
								</Text>
								<Text
									style={{ fontFamily: Theme.FontFamily.normal, color: Theme.colors.primary, fontSize: Theme.sizes.tiny }}
								>
									Tell us about your registration informations and state.
								</Text>
							</View>
						</View>

						<View style={{ flexDirection: 'row', marginBottom: 20,paddingTop:20 }}>
							<View>
								<Text
									style={{ fontFamily: Theme.FontFamily.bold, color: Theme.colors.black, fontSize: Theme.sizes.h6 }}
								>
									04.
								</Text>
							</View>
							<View style={{ paddingLeft: 5 }}>
								<Text
									style={{ fontFamily: Theme.FontFamily.bold, color: Theme.colors.black, fontSize: Theme.sizes.h6, paddingBottom: 5 }}
								>
									Set Up Your Loction
								</Text>
								<Text
									style={{ fontFamily: Theme.FontFamily.normal, color: Theme.colors.primary, fontSize: Theme.sizes.tiny }}
								>
									Setting your location to get best results of your search
								</Text>
							</View>
						</View>

						<View style={{ flexDirection: 'row',paddingTop:20 }}>
							<TouchableOpacity 
								onPress={()=>{setAcceptTerms(!acceptTerms)}}
								style={{ justifyContent: 'center' }}>
								<View style={[acceptTerms == true ? activeTermsStyle.outerStyle : defaultTermsStyle.outerStyle, { borderWidth: 1, justifyContent: 'center', alignSelf: 'center' }]}>
									<Icon name="checkmark-outline" size={20} style={[acceptTerms == true ? activeTermsStyle.iconStyle : defaultTermsStyle.iconStyle,{ fontWeight: '600' }]} />
								</View>
							</TouchableOpacity>
							<View style={{ justifyContent: 'center', flex: 1, paddingLeft: 10 }}>
								<Text
									style={{ fontFamily: Theme.FontFamily.normal, color: Theme.colors.black, fontSize: Theme.sizes.tiny }}
								>
									By clicking on the checkbox, you <Text style={{ color: Theme.colors.primary }}>‘agree to’</Text> our 
									<TouchableOpacity onPress={()=>{props.navigation.navigate('TermsScreen')}}>
										<Text style={{ fontFamily: Theme.FontFamily.bold,fontSize:Theme.sizes.h7,color:Theme.colors.black }}>
											Terms and Conditions
										</Text>
									</TouchableOpacity>
								</Text>
							</View>

						</View>
						

						<View style={{ marginTop: 30,paddingBottom:20 }}>
							<BlackButton style={{ height: 45 }} buttonText="CONFIRM" 
							onSubmitPress={() => { 
								// props.navigation.navigate('TermsScreen') 
								props.navigation.navigate('CongratulationScreen') 
							}} />
						</View>

					</View>
				</View>

			</View>
		</ScreenLayout>


	)
}

export default PermissionInfoScreen;
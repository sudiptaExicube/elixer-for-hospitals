import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	Pressable
} from 'react-native';
import React, { useRef, useState } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../constants';
import { BlackButton, DropdownPicker, EditTextInputBox, ScreenLayout } from '../../Components';
// import * as Animatable from 'react-native-animatable';
// import Permission from '../../assets/images/permission.svg';
import Style from './Style';
// import { Rating, AirbnbRating } from 'react-native-ratings';
// import { TextInput } from 'react-native';
import NotificationPermission from '../../assets/images/notification_permission.svg'
import MapPermission from '../../assets/images/map_permission.svg'
import { ScrollView } from 'react-native';
import { FlatList } from 'react-native';
import { Image } from 'react-native';
import ModalBottom from '../../Components/ModalBottom/ModalBottom';
import { Calendar } from 'react-native-calendars';
import DatePicker from 'react-native-date-picker';
import { Button } from 'react-native';
import moment from "moment";

function CreateScheduleScreen(props) {
	const Styles = Style()
	const onPress = () => HelperFunctions.sampleFunction('its working fine');

	const selectedColor = { borderColor: Theme.colors.primary, color: Theme.colors.white, backgroundColor: Theme.colors.primary, letterSpacing: 0.5, fontFamily: Theme.FontFamily.medium, fontWeight: '500' }
	const defaultColor = { borderColor: Theme.colors.background_shade1, color: Theme.colors.black, backgroundColor: Theme.colors.background_shade1, letterSpacing: 0.5, fontFamily: Theme.FontFamily.normal, fontWeight: '400' }

	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */

	const [selectedIndex, setSelectedIndex] = useState(null);
	const [userValue, setuserValue] = useState(null);
	const [timeslotValue, settimeslotValue] = useState(null);

	const specialityDetails = [
		{ value: 'ICU' }, { value: 'Emergency' }, { value: 'Consultation' }
	]
	const userTypeDetails = [
		{ value: 'Doctors' }, { value: 'Nurse' }
	]
	const timeslotDetails = [
		{ value: '8am - 12pm' },
		{ value: '12pm - 4pm' },
		{ value: '4pm - 8pm' },
		{ value: '8pm - 12am' },
		{ value: '12am - 4am' },
		{ value: '4am - 8am' }
	]

	/* == Render menu item return function ==*/
	const renderItem = ({ item, index }) => (
		<TouchableOpacity style={{}} onPress={() => { setSelectedIndex(index) }}>
			<View style={[selectedIndex == index ? selectedColor : defaultColor, { minWidth: 50, padding: 5, paddingHorizontal: 8, borderWidth: 1, borderRadius: 5, margin: 5, justifyContent: 'center', flexDirection: 'column' }]}>
				<Text style={[selectedIndex == index ? selectedColor : defaultColor, { fontSize: Theme.sizes.h7, textAlign: 'center' }]}>{item?.value}</Text>
			</View>
		</TouchableOpacity>
	)

	/* == Render menu item return function ==*/
	const showUserLists = ({ item, index }) => (
		<TouchableOpacity style={{}} onPress={() => { setuserValue(index) }}>
			<View style={[userValue == index ? selectedColor : defaultColor, { minWidth: 50, padding: 5, paddingHorizontal: 8, borderWidth: 1, borderRadius: 5, margin: 5, justifyContent: 'center', flexDirection: 'column' }]}>
				<Text style={[userValue == index ? selectedColor : defaultColor, { fontSize: Theme.sizes.h7, textAlign: 'center' }]}>{item?.value}</Text>
			</View>
		</TouchableOpacity>
	)

	/* == Render menu item return function ==*/
	const showTimeSlot = ({ item, index }) => (
		<TouchableOpacity style={{}} onPress={() => { settimeslotValue(index) }}>
			<View style={[timeslotValue == index ? selectedColor : defaultColor, { minWidth: 50, padding: 5, paddingHorizontal: 8, borderWidth: 1, borderRadius: 5, margin: 5, justifyContent: 'center', flexDirection: 'column' }]}>
				<Text style={[timeslotValue == index ? selectedColor : defaultColor, { fontSize: Theme.sizes.h7, textAlign: 'center' }]}>{item?.value}</Text>
			</View>
		</TouchableOpacity>
	)

	const clearFilter = () => {
		setSelectedIndex(null);
		setuserValue(null);
		settimeslotValue(null);
		setTimeout(() => {
			props.navigation.goBack();
		}, 200);
	}

	const applyFilter = () => {
		setTimeout(() => {
			props.navigation.goBack();
		}, 200);
	}

	// const [selectedIndex, setSelectedIndex] = useState(null);

	const [documentBottomVisible, setDocumentBottomVisible] = useState(false)
	const [selectedDocumentType, setSelectedDocumentType] = useState('Schedule type')

	const closeDocumentType = () => {
		setDocumentBottomVisible(false)
	}

	const changeScheduleType = (value) => {
		// console.log("Value  :", value);
		setSelectedDocumentType(value);
		setDocumentBottomVisible(false)

	}

	const [startDate, setStartDate] = useState(new Date())
	const [showStartDateModal, setShowStartDateModal] = useState(false)

	const [endDate, setEndDate] = useState(new Date())
	const [showEndDateModal, setShowEndDateModal] = useState(false)

	const [startTime, setStartTime] = useState(new Date())
	const [showStartTimeModal, setShowStartTimeModal] = useState(false)

	const [endTime, setEndTime] = useState(new Date())
	const [showEndTimeModal, setShowEndTimeModal] = useState(false)




	return (

		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={'Create Schedule'}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={Theme.colors.primary}
			showHeaderLine={false}
			// customBackground={Theme.colors.white}
			customBackground={'#F8FBFF'}
			isBackBtn={true}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				props.navigation.goBack();

			}}
			backButton_animationRef={viewRef}
			showScrollView={true}
		>
			<View style={Styles.mainView}>
				<View style={{ height: 80, width: '100%', backgroundColor: Theme.colors.primary }}>
					<View style={{ width: '90%', alignSelf: 'center', backgroundColor: 'white', height: 100 }}></View>
				</View>
				<View style={{ position: 'absolute', top: 0, height: WindowData.windowHeight - 60, width: '90%', backgroundColor: Theme.colors.white, alignSelf: 'center', paddingVertical: 10 }}>
					<View style={{ paddingHorizontal: 20 }}>
						<Text style={{ fontFamily: Theme.FontFamily.bold, letterSpacing: 0.5,textAlign:'center',fontSize:Theme.sizes.h4,color:Theme.colors.black }}>Create Schedule</Text>
					</View>
					<ScrollView style={{ flex: 1 }}>
						{/* Schedule type */}
						<View style={{paddingHorizontal:20,marginTop:'10%'}}>
							<Text style={{paddingBottom:8,fontFamily:Theme.FontFamily.bold,color:Theme.colors.primary,letterSpacing:0.5}}>
								Start Time
							</Text>
						</View>
						<DropdownPicker
							selectedValue={selectedDocumentType ? selectedDocumentType : 'Schedule type'}
							setDocumentBottomVisible={() => {
								setDocumentBottomVisible(true)
							}}
							style={{ minHeight: 45 }}
						/>
						<ModalBottom modalCloseAction={() => { closeDocumentType() }}
							modalHeaderText={'Schedule type'}
							modalHeight={300}
							modalVisible={documentBottomVisible}>
							<View style={{}}>
								<TouchableOpacity
									onPress={() => { changeScheduleType('ICU') }}
									style={{ padding: 10, borderBottomColor: Theme.colors.background_shade1, borderBottomWidth: 2, flexDirection: 'row', justifyContent: 'space-between' }}>
									<View style={{ flexDirection: 'row' }}>
										<View style={{ justifyContent: 'center', paddingRight: 6 }}><Icon name="radio-button-on-outline" color={Theme.colors.primary} /></View>
										<Text style={{ color: Theme.colors.black, fontFamily: Theme.FontFamily.bold, letterSpacing: 0.5, fontSize: Theme.sizes.h5 }}>ICU</Text>
									</View>
									<Icon name="chevron-forward-outline" size={20} />
								</TouchableOpacity>

								<TouchableOpacity
									onPress={() => { changeScheduleType('Emergency') }}
									style={{ padding: 10, borderBottomColor: Theme.colors.background_shade1, borderBottomWidth: 2, flexDirection: 'row', justifyContent: 'space-between' }}>
									<View style={{ flexDirection: 'row' }}>
										<View style={{ justifyContent: 'center', paddingRight: 6 }}><Icon name="radio-button-on-outline" color={Theme.colors.primary} /></View>
										<Text style={{ color: Theme.colors.black, fontFamily: Theme.FontFamily.bold, letterSpacing: 0.5, fontSize: Theme.sizes.h5 }}>Emergency</Text>
									</View>
									<Icon name="chevron-forward-outline" size={20} />
								</TouchableOpacity>

								<TouchableOpacity
									onPress={() => { changeScheduleType('Consultation') }}
									style={{ padding: 10, borderBottomColor: Theme.colors.background_shade1, borderBottomWidth: 2, flexDirection: 'row', justifyContent: 'space-between' }}>
									<View style={{ flexDirection: 'row' }}>
										<View style={{ justifyContent: 'center', paddingRight: 6 }}><Icon name="radio-button-on-outline" color={Theme.colors.primary} /></View>
										<Text style={{ color: Theme.colors.black, fontFamily: Theme.FontFamily.bold, letterSpacing: 0.5, fontSize: Theme.sizes.h5 }}>Consultation</Text>
									</View>
									<Icon name="chevron-forward-outline" size={20} />
								</TouchableOpacity>

							</View>
						</ModalBottom>

						{/* Schedule type */}

						{/* <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 23, marginTop: 20 }}>
							<View style={{flex:1}}>
								<Text style={{paddingBottom:8,fontFamily:Theme.FontFamily.bold,color:Theme.colors.primary,letterSpacing:0.5}}>
									Start Date
								</Text>
								<TouchableOpacity style={{ flex: 1 }}
									onPress={() => {setShowStartDateModal(true)}}
								>
									<View style={{ minHeight: 45, borderWidth: 1, borderColor: Theme.colors.primary, borderRadius: 6, justifyContent: 'center' }}>
										<Text style={{ paddingLeft: 20, fontFamily: Theme.FontFamily.medium, letterSpacing: 0.5, fontSize: 12 }}>
											{startDate ? moment(startDate).format("DD-MM-YYYY") : 'Start Date'}
										</Text>
									</View>
								</TouchableOpacity>
							</View>

							<View style={{ width: 20 }}></View>


							<View style={{flex:1}}>
								<Text style={{paddingBottom:8,fontFamily:Theme.FontFamily.bold,color:Theme.colors.primary,letterSpacing:0.5}}>
									End Date
								</Text>
								<TouchableOpacity style={{ flex: 1 }}
									onPress={() => { setShowEndDateModal(true)}}
								>
									<View style={{ minHeight: 45, borderWidth: 1, borderColor: Theme.colors.primary, borderRadius: 6, justifyContent: 'center' }}>
										<Text style={{ paddingLeft: 20, fontFamily: Theme.FontFamily.medium, letterSpacing: 0.5, fontSize: 12 }}>
											{endDate ? moment(endDate).format("DD-MM-YYYY") : 'End Date'}
										</Text>
									</View>
								</TouchableOpacity>
							</View>

						</View> */}

						{/* Start Date picker */}
						<DatePicker
							modal open={showStartDateModal} date={startDate}
							onConfirm={(date) => {
								console.log("date : ",date)
								setShowStartDateModal(false)
								setStartDate(date);
							}}
							onCancel={() => { setShowStartDateModal(false) }}
							mode='date'
						/>

						{/* End Date picker */}
						<DatePicker
							modal open={showEndDateModal} date={endDate}
							onConfirm={(date) => {
								console.log("date : ",date)
								setShowEndDateModal(false)
								setEndDate(date);
							}}
							onCancel={() => { setShowEndDateModal(false) }}
							mode='date'
						/>

						<View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 23, marginTop: 20 }}>
							<View style={{flex:1}}>
								<Text style={{paddingBottom:8,fontFamily:Theme.FontFamily.bold,color:Theme.colors.primary,letterSpacing:0.5}}>
									Start Time
								</Text>
								<TouchableOpacity style={{ flex: 1 }}
									onPress={() => {setShowStartTimeModal(true)}}
								>
									<View style={{ minHeight: 45, borderWidth: 1, borderColor: Theme.colors.primary, borderRadius: 6, justifyContent: 'center' }}>
										<Text style={{ paddingLeft: 20, fontFamily: Theme.FontFamily.medium, letterSpacing: 0.5, fontSize: 12,color:Theme.colors.black }}>
											{startTime ? moment(startTime).format("hh:mm A") : 'Start Time'}
										</Text>
									</View>
								</TouchableOpacity>
							</View>

							<View style={{ width: 20 }}></View>


							<View style={{flex:1}}>
								<Text style={{paddingBottom:8,fontFamily:Theme.FontFamily.bold,color:Theme.colors.primary,letterSpacing:0.5}}>
									End Time
								</Text>
								<TouchableOpacity style={{ flex: 1 }}
									onPress={() => { setShowEndTimeModal(true)}}
								>
									<View style={{ minHeight: 45, borderWidth: 1, borderColor: Theme.colors.primary, borderRadius: 6, justifyContent: 'center' }}>
										<Text style={{ paddingLeft: 20, fontFamily: Theme.FontFamily.medium, letterSpacing: 0.5, fontSize: 12,color:Theme.colors.black }}>
											{endTime ? moment(endTime).format("hh:mm A") : 'End Time'}
										</Text>
									</View>
								</TouchableOpacity>
							</View>

						</View>
						{/* Start Time picker */}
						<DatePicker
							modal open={showStartTimeModal} date={startTime}
							onConfirm={(date) => {
								console.log("date : ",date)
								setShowStartTimeModal(false)
								setStartTime(date);
							}}
							onCancel={() => { setShowStartTimeModal(false) }}
							mode='time'
						/>

						{/* End Time picker */}
						<DatePicker
							modal open={showEndTimeModal} date={endTime}
							onConfirm={(date) => {
								console.log("date : ",date)
								setShowEndTimeModal(false)
								setEndTime(date);
							}}
							onCancel={() => { setShowEndTimeModal(false) }}
							mode='time'
						/>

						<View style={{marginTop:20}}>
							<Text style={{paddingHorizontal:22, paddingBottom:8,fontFamily:Theme.FontFamily.bold,color:Theme.colors.primary,letterSpacing:0.5}}>
									Set Price
								</Text>
							<EditTextInputBox 
							keyboardType={'numeric'}
							placeHolderText="Set price for this time frame" style={{minHeight:45}}/>
						</View>






					</ScrollView>

					<View style={{ height: 150, flexDirection: 'row', justifyContent: 'space-evenly' }}>
						{/* <View style={{ flex: 1 }}>
							<BlackButton buttonText={'CLEAR'} onSubmitPress={() => { clearFilter() }} style={{ height: 44, backgroundColor: Theme.colors.background_shade1, borderColor: Theme.colors.background_shade1 }} textColor={Theme.colors.black} textCustomStyle={{ letterSpacing: 0.8 }} />
						</View> */}
						<View style={{ flex: 1 }}>
							<BlackButton buttonText={'CREATE SCHEDULE'} onSubmitPress={() => { applyFilter() }} style={{ height: 44 }} textColor={Theme.colors.primary} textCustomStyle={{ letterSpacing: 0.8 }} />
						</View>
					</View>

				</View>


			</View>


		</ScreenLayout>
	)
}

export default CreateScheduleScreen;
import { View, Text, SafeAreaView } from 'react-native'
import React, { useRef } from 'react'
import HeaderLine from '../../Components/HeaderLine/HeaderLine'
import { BlackButton, EditTextInputBox, Header, ScreenLayout } from '../../Components'
import BackHeader from '../../Components/BackHeader/BackHeader'
import Style from './Style'

import ForgotIcon from '../../assets/images/ForgotPasswordRafiki1.svg'
import { Theme } from '../../constants'

const Styles=Style();

const ForgotPasswordScreen = () => {
  const viewRef = useRef(null);
  const showBounceAnimation = (value) => {
    value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
    value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
  }

  return (
    <ScreenLayout
    isHeaderShown={true}
    isHeaderLogo={false}
    hTitle={"Forgot Password"}
    headerBackground={Theme.colors.white}
    headerStyle={{width: '94%',alignSelf: 'center'}}
    showHeaderLine={true}
    customBackground={Theme.colors.secondaryBackground}

    isBackBtn={true}
    backBtnIcnName={"chevron-back-outline"}
    backBtnFunc={() => {
      showBounceAnimation(viewRef)
    }}
    backButton_animationRef={viewRef}
    >
      <View style={Styles.containerBox}>
       <ForgotIcon style={{alignSelf:'center',marginTop:'14%'}}/>
       <EditTextInputBox style={{paddingStart:4,backgroundColor:'#D9D9D924'}} placeHolderText="Enter your Email / Mobile number"/>
       <Text style={Styles.otpText}>An OTP will be sent to your email / phone no to set your new password.</Text>

       <BlackButton style={{marginTop:'25%'}} buttonText="SENT"/>
       </View>
       </ScreenLayout>
  )
}

export default ForgotPasswordScreen
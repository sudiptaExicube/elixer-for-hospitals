import { View, Text, TouchableOpacity, Switch, Pressable } from 'react-native';
import React, { useRef, useState } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  GlobalStyles,
  HelperFunctions,
  Theme,
  WindowData,
} from '../../constants';
import { BlackButton, DropdownPicker, EditTextInputBox, ScreenLayout } from '../../Components';
import Style from './Style';
import { ScrollView } from 'react-native';
import { FlatList } from 'react-native';
import { Image } from 'react-native';
// import DoctorIcon from '../../assets/images/DoctorIcon.svg'
import CameraIcon from '../../assets/images/Camera.svg';

import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import { CheckBox } from '@rneui/themed';
import ModalBottom from '../../Components/ModalBottom/ModalBottom';
import IdCard from '../../assets/images/IdCard.svg';

const ProfileScreen = props => {
  const Styles = Style();
  const onPress = () => HelperFunctions.sampleFunction('its working fine');
  const [Gender, setGenderCheckBox] = useState(null)

  // const [locationToggleValue, setLocationToggleValue] = useState(false);
  // const [notifiationToggleValue, setNotifiationToggleValue] = useState(false);

  /* For Header customization Animation */
  const viewRef = useRef(null);
  const viewRef1 = useRef(null);
  const viewRef2 = useRef(null);
  const viewRef3 = useRef(null);
  const showBounceAnimation = value => {
    value.current.animate({
      0: { scale: 1, rotate: '0deg' },
      1: { scale: 1.7, rotate: '0deg' },
    });
    value.current.animate({
      0: { scale: 1.7, rotate: '0deg' },
      1: { scale: 1, rotate: '0deg' },
    });
  };
  const [documentBottomVisible, setDocumentBottomVisible] = useState(false)
  const [selectedDocumentType, setSelectedDocumentType] = useState("")

  const closeDocumentType = () => {
    setDocumentBottomVisible(false)
  }

  /* For Header customization Animation END */

  return (
    <ScreenLayout
      isHeaderShown={true}
      isHeaderLogo={false}
      hTitle={'Profile'}
      // headerStyle={{backgroundColor:Theme.colors.primary}}
      headerBackground={Theme.colors.primary}
      showHeaderLine={false}
      // customBackground={Theme.colors.white}
      customBackground={'#F8FBFF'}
      isBackBtn={true}
      backBtnIcnName={'chevron-back-outline'}
      backBtnFunc={() => {
        showBounceAnimation(viewRef);
        props.navigation.goBack();
      }}
      backButton_animationRef={viewRef}
      showScrollView={true}>
      <View style={Styles.mainView}>
        <View
          style={{
            height: 80,
            width: '100%',
            backgroundColor: Theme.colors.primary,
          }}>
          <View
            style={{
              width: '90%',
              alignSelf: 'center',
              backgroundColor: 'white',
              height: 100,
            }}></View>
        </View>
        <View
          style={{
            position: 'absolute',
            top: 0,
            height: WindowData.windowHeight - 60,
            width: '90%',
            backgroundColor: Theme.colors.white,
            alignSelf: 'center',
            paddingHorizontal: 15,
            paddingVertical: 10,
          }}>
          <ScrollView style={{ flex: 1 }}>
            <SimpleLineIcons
              style={{ alignSelf: 'flex-end' }}
              size={18}
              name={'pencil'}
              color={Theme.colors.black}
            />
            <View
              style={{
                borderRadius: 79 / 2,
                borderWidth: 1,
                borderColor: '#BFA058',
                width: 79,
                aspectRatio: 1,
                backgroundColor: '#E7E7E7',
                alignSelf: 'center',
                alignItems: 'center',
                justifyContent: 'flex-end',
              }}>
              <Image
                source={require('../../assets/images/doctor.png')}
                style={{
                  //   position: 'absolute',
                  resizeMode: 'contain',
                  height: 80,
                  //   width: 200,
                  //   bottom: -6,
                }}
              />
              <View style={{
                backgroundColor: 'white', position: 'absolute', bottom: -5, right: -4, paddingHorizontal: 5, paddingVertical: 4, borderRadius: 5
              }}>
                <Image
                  source={require('../../assets/images/cameraIcon.png')}
                  style={{
                    // backgroundColor: Theme.colors.white,
                    padding: 5,
                    alignSelf: 'flex-end',
                    height: 18, width: 18,
                  }}
                />
              </View>
            </View>

            <Text style={Styles.doctorName}>Dr. John duo</Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                width: '100%',
                alignSelf: 'center',
                marginTop: '8%',
              }}>
              <Text style={Styles.text}>Gender</Text>
              <CheckBox
                marginStart="19%"
                marginEnd="-1%"
                checked={Gender === 0}
                onPress={() => setGenderCheckBox(0)}
                checkedColor="#000"
                uncheckedColor="#000"
                iconType="material-community"
                checkedIcon="checkbox-outline"
                uncheckedIcon={'checkbox-blank-outline'}
                size={18}
              />
              <Text style={Styles.text}>Male</Text>
              <Image
                style={{ marginStart: '2%' }}
                source={require('../../assets/images/MaleIcon.png')}
              />

              <CheckBox
                marginEnd="-1%"
                checked={Gender === 1}
                onPress={() => setGenderCheckBox(1)}
                checkedColor="#000"
                uncheckedColor="#000"
                iconType="material-community"
                checkedIcon="checkbox-outline"
                uncheckedIcon={'checkbox-blank-outline'}
                size={18}
              />
              <Text style={Styles.text}>Female</Text>
              <Image
                style={{ marginStart: '2%' }}
                source={require('../../assets/images/FemaleIcon.png')}
              />
            </View>
            <EditTextInputBox placeHolderText="yourmailaddress@gmail.com" style={{ marginTop: 20, height: 55, width: '100%' }} />
            <EditTextInputBox keyboardType={'numeric'} placeHolderText="+00 123 456 7890" style={{ marginTop: 20, height: 55, width: '100%' }} />

            <DropdownPicker
              selectedValue={'Document Type'}
              setDocumentBottomVisible={() => {
                setDocumentBottomVisible(true)
                setSelectedDocumentType("Document Type")
              }}
              style={{ marginTop: 20, height: 55, width: '100%' }} />

            <ModalBottom modalCloseAction={() => { closeDocumentType() }}
              modalHeaderText={selectedDocumentType}
              modalVisible={documentBottomVisible}>
              <View style={{ paddingTop: 40 }}>
                <Text style={{ fontSize: Theme.sizes.h5, lineHeight: 25, textAlign: 'center', fontFamily: Theme.FontFamily.medium, color: 'black' }}>Data should be dynamic, and We will implement in the time of development phase</Text>
              </View>
            </ModalBottom>



            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, justifyContent: 'space-around' }}>
              <Text style={Styles.text}>Upload Document Image</Text>
              <Image source={require('../../assets/images/IdCard.png')} style={{ resizeMode: 'contain', height: 77, width: 120 }} />
            </View>
          </ScrollView>
        </View>
      </View>
    </ScreenLayout>
  );
};

export default ProfileScreen;

import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	Pressable,
	Platform
} from 'react-native';
import React, { useRef, useState } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../constants';
import { BlackButton, ScreenLayout } from '../../Components';
// import * as Animatable from 'react-native-animatable';
// import Permission from '../../assets/images/permission.svg';
import Style from './Style';
// import { Rating, AirbnbRating } from 'react-native-ratings';
// import { TextInput } from 'react-native';
import NotificationPermission from '../../assets/images/notification_permission.svg'
import MapPermission from '../../assets/images/map_permission.svg'
import { ScrollView } from 'react-native';
import { FlatList } from 'react-native';
import { Image } from 'react-native';


function RequestlistScreen(props) {
	const Styles = Style()
	const onPress = () => HelperFunctions.sampleFunction('its working fine');

	// const [locationToggleValue, setLocationToggleValue] = useState(false);
	// const [notifiationToggleValue, setNotifiationToggleValue] = useState(false);

	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */

	const teamDetails=[
		{ name: 'Orthopedic', staff_type:'Doctor', date:'20-06-22', time:'12am - 8am',type: 'Emergency', designation: 'Developer', imageUrl: require('../../assets/images/doctor_profile.png') },
		{ name: 'Orthopedic', staff_type:'Doctor', date:'20-06-22', time:'12am - 8am',type: 'Emergency', designation: 'Developer', imageUrl: require('../../assets/images/doctor_profile.png') },
		{ name: 'Orthopedic', staff_type:'Doctor', date:'20-06-22', time:'12am - 8am',type: 'Emergency', designation: 'Developer', imageUrl: require('../../assets/images/doctor_profile.png') },
		{ name: 'Orthopedic', staff_type:'Doctor', date:'20-06-22', time:'12am - 8am',type: 'Emergency', designation: 'Developer', imageUrl: require('../../assets/images/doctor_profile.png') },
		{ name: 'Orthopedic', staff_type:'Doctor', date:'20-06-22', time:'12am - 8am',type: 'Emergency', designation: 'Developer', imageUrl: require('../../assets/images/doctor_profile.png') },
	]

	/* == Render menu item return function ==*/
	const renderItem = ({ item }) => (
		<View style={{}}>
			<TouchableOpacity style={{}}
				onPress={()=>{
					// props.navigation.navigate('AppointmentDetailsScreen',{item: item})
				}}
			>
				<View style={{paddingHorizontal:10,backgroundColor:'#FAFAFA',marginBottom:15,  paddingVertical: 10, width: '100%',
				borderWidth:1,borderColor:Theme.colors.background_shade1,borderRadius:10
			}}>
					<View style={{flex: 1, flexDirection: 'row'}}>

						{/* <View style={{ justifyContent: 'center', flexDirection: 'column' }}>
							<View style={{ height: 75, width: 75, borderRadius: 75, justifyContent: 'center', flexDirection: 'column' }}>
								<Image source={item?.imageUrl} style={{ height: 74, width: 74, borderRadius: 74, resizeMode: 'cover', borderColor: Theme.colors.primary, borderWidth: 1 }} />
							</View>
						</View> */}
						<View style={{ flex: 1, justifyContent: 'center', flexDirection: 'column' }}>
							<View style={{ paddingHorizontal: 10, justifyContent: 'center', flexDirection: 'column' }}>
								<Text style={[Styles.itemText, { fontFamily: Theme.FontFamily.bold, paddingBottom: 3, fontSize: Theme.sizes.h5, flexWrap: 'wrap', color: Theme.colors.black, letterSpacing: 0.5,
									textShadowColor: 'rgba(0, 0, 0, 0.25)',
									textShadowOffset: { width: 0, height: 4 },
									textShadowRadius: 4,
									textTransform:'capitalize'
								}]}>{item?.name}</Text>
								<View style={{ flexDirection: 'row',paddingTop:5 }}>
										<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.primary }]}>Specialist: </Text>
										<Text style={[Styles.itemText, {
											paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.black,
											textShadowColor: 'rgba(0, 0, 0, 0.25)',
											textShadowOffset: { width: 0, height: 4 },
											textShadowRadius: 4,
										}]}>{item?.mode} {item?.type}</Text>
								</View>
								<View style={{ flexDirection: 'row',paddingTop:5 }}>
										<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.primary }]}>Staff: </Text>
										<Text style={[Styles.itemText, {
											paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.black,
											textShadowColor: 'rgba(0, 0, 0, 0.25)',
											textShadowOffset: { width: 0, height: 4 },
											textShadowRadius: 4,
										}]}>{item?.staff_type}</Text>
								</View>
								<View style={{ flexDirection: 'row',justifyContent:'space-between',paddingTop:5 }}>
									<View style={{ flexDirection: 'row' }}>
											<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.primary }]}>Date: </Text>
											<Text style={[Styles.itemText, {
												paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.black,
											}]}>{item?.date}</Text>
										</View>
										<View style={{ flexDirection: 'row' }}>
											<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.primary }]}>Time: </Text>
											<Text style={[Styles.itemText, {
												paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.black,
											}]}>{item?.time}</Text>
										</View>
								</View>
							</View>
							<View style={{flexDirection:'row',justifyContent:'center',paddingTop:10}}>
								<TouchableOpacity 
									onPress={()=>{ props.navigation.goBack()}}
									style={{flex:1,borderWidth:1,backgroundColor:Theme.colors.background_shade1,
								paddingHorizontal:10,paddingVertical:10,borderRadius:6,width:'100%'}}>
									<Text style={{color:Theme.colors.black,textAlign:'center',letterSpacing:0.5}}>Reject</Text>
								</TouchableOpacity>
								<View style={{width:20}}></View>
								<TouchableOpacity 
									onPress={()=>{ props.navigation.goBack()}}
									style={{flex:1,borderWidth:1,backgroundColor:Theme.colors.black,
								paddingHorizontal:10,paddingVertical:10,borderRadius:6,width:'100%'}}>
									<Text style={{color:Theme.colors.primary,textAlign:'center',letterSpacing:0.5}}>Accept</Text>
								</TouchableOpacity>
							</View>
						</View>
						
					</View>			
					
				</View>

			</TouchableOpacity>
		</View>
	)





	return (

		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={'Requests'}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={Theme.colors.primary}
			showHeaderLine={false}
			// customBackground={Theme.colors.white}
			customBackground={'#F8FBFF'}
			isBackBtn={true}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				props.navigation.goBack();

			}}
			backButton_animationRef={viewRef}
			showScrollView={true}
		>
			<View style={Styles.mainView}>
				<View style={{ height: 80, width: '100%', backgroundColor: Theme.colors.primary }}>
					<View style={{width:'90%',alignSelf:'center',backgroundColor:'white',height:100}}></View>
				</View>
				<View style={{ position: 'absolute', top: 0, height: WindowData.windowHeight-60, width: '90%', backgroundColor: Theme.colors.white, alignSelf: 'center', paddingHorizontal: 15, paddingVertical: 10 }}>
					<ScrollView style={{ flex: 1 }}
						showsVerticalScrollIndicator={false}
						showsHorizontalScrollIndicator={false}
					>
						{/* <View style={{paddingBottom:10}}>
							<Text 
								style={{fontFamily:Theme.FontFamily.medium,fontSize:Theme.sizes.h5,color:Theme.colors.black,fontWeight: '600'}}
							>Request lists</Text>
						</View> */}

						<View style={{paddingTop:0}}>
								<FlatList
									// horizontal={true}
									data={teamDetails}
									renderItem={renderItem}
									// keyExtractor={(item, index) => item.id}
									keyExtractor={(item, index) => index}
									showsVerticalScrollIndicator={false}
									showsHorizontalScrollIndicator={false}
								// style={{paddingTop:30}}
								/>		

						</View>
						
					</ScrollView>

					{Platform.OS == 'ios' ?
						<View style={{ paddingBottom: '15%' }}></View>
					: null}

				</View>
				


			</View>


		</ScreenLayout>
	)

}

export default RequestlistScreen;
import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	ScrollView, Image
} from 'react-native';
import React, { useRef, useState } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../constants';

import Style from './Style';
import { BlackButton, EditTextInputBox, ScreenLayout } from '../../Components';
import CancelIcon from '../../assets/images/cancelled_icon.svg';
import KeyIcon from '../../assets/images/Key.svg'

const SetPasswordScreen = props => {
	const Styles = Style()
	const globalstyles = GlobalStyles();
	const onPress = () => HelperFunctions.sampleFunction('its working fine');

	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */
	const hospitalIconImage = require('../../assets/images/hospital_icon.png');

	const [selectedIndex, setSelectedIndex] = useState(null);
	const [iconName, setIconName] = useState('eye-outline');
	const [hidePassword, setHidePassword] = useState(true);


	const teamDetails = [
		{ value: '8am to 12pm' },
		{ value: '12pm to 4pm' },
		{ value: '4pm to 8pm' },
		{ value: '8pm to 12am' },
		{ value: '12am to 4am' },
		{ value: '4am to 8am' }
	]

	return (


		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={''}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={'#F8FBFF'}
			// headerBackground={'red'}
			showHeaderLine={false}
			// customBackground={Theme.colors.white}
			customBackground={'#F8FBFF'}
			// customBackground={'red'}
			isBackBtn={true}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				props.navigation.goBack();

			}}
			backButton_animationRef={viewRef}
			showScrollView={false}
		>
			<View style={Styles.mainView}>
				<View style={{
					height: WindowData.customBody_height, width: '90%', backgroundColor: Theme.colors.white, alignSelf: 'center', paddingTop: '40%'
				}}>

					<View>
						<View style={{ paddingLeft: 20, paddingBottom: 30 }}>
							<Text style={{ fontFamily: Theme.FontFamily.medium, color: Theme.colors.black }}>Set your 4 digit password</Text>
						</View>
						<View style={{ marginBottom: 30 }}>
							<EditTextInputBox secureTextEntry={hidePassword} style={{ height: 45, backgroundColor: '#D9D9D924' }}
								icon={<KeyIcon style={{ marginStart: '4%', marginEnd: '1%' }} />}
								placeHolderText="Set password"
								rightIcon={<Icon name={iconName} size={20} style={{ paddingRight: 10 }}
									onPress={() => { setIconName(iconName == 'eye-outline' ? 'eye-off-outline' : 'eye-outline'); setHidePassword(!hidePassword) }}
								/>}
							/>
						</View>
						<View style={{ marginBottom: 30 }}>
							<EditTextInputBox secureTextEntry={true} style={{ height: 45, backgroundColor: '#D9D9D924' }} icon={<KeyIcon style={{ marginStart: '4%', marginEnd: '1%' }} />} placeHolderText="Confirm password" />
						</View>
						<View>
							<BlackButton style={{ height: 45 }} buttonText="SAVE"

								// onSubmitPress={()=>{props.navigation.replace('CongratulationScreen')}}
								onSubmitPress={() => { props.navigation.replace('IdentityVerification') }}
							/>
						</View>
					</View>

				</View>


			</View>


		</ScreenLayout>


	)
}

export default SetPasswordScreen;
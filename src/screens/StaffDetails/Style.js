//import liraries
import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import { Theme } from '../../constants';
// import { useTheme } from '../../../Constants/Theme/Theme';


// create a component
const Styles = () => {
  // const {colorTheme} = useTheme();
  return StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      paddingLeft:10,
      paddingRight:10
    },

    itemText:{
      fontFamily:Theme.FontFamily.medium,
      fontSize:Theme.sizes.h5,
      color:Theme.colors.black,
    },
    mainView:{
      width:'100%', alignSelf:'center',
    // height:WindowData.windowHeight-143,
      flex:1,
      //paddingHorizontal:20
    },





  });

};
export default Styles;
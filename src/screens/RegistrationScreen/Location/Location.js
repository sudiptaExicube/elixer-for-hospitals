import { View, Text, TouchableOpacity } from 'react-native'
import React, { useRef, useState } from 'react'
import { BlackButton, DropdownPicker, EditTextInputBox, ScreenLayout } from '../../../Components';
import { Theme } from '../../../constants';
import { windowHeight, windowWidth } from '../../../constants/GlobalStyles';
import Style from './Style';
import Icon from 'react-native-vector-icons/Ionicons';
import ModalBottom from '../../../Components/ModalBottom/ModalBottom';

const Styles = Style()

const LocationScreen = (props) => {
  const [selectedDistrictVisible, setDistrictBottomVisible] = useState(false)
  const [selectedDistrict, setSelectedDistrict] = useState("Select District")

  const [selectedStateVisible, setSelectedStateVisible] = useState(false)
  const [selectedState, setSelectedState] = useState("Select state")

  const [selectedCountryVisible, setSelectedCountryVisible] = useState(false)
  const [selectedCountry, setSelectedCountry] = useState("Country")

  const viewRef = useRef(null);
  const showBounceAnimation = (value) => {
    value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
    value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
  }

  const [documentBottomVisible, setDocumentBottomVisible] = useState(false)
  const [selectedDocumentType, setSelectedDocumentType] = useState("")

  const closeDocumentType = () => {
    setDocumentBottomVisible(false)
  }

  const closeDistrictName = () => {
    setDistrictBottomVisible(false)
  }
  const closeState = () => {
    setSelectedStateVisible(false)
  }

  const closeCountry = () => {
    setSelectedStateVisible(false)
  }

  const clickRegistrationSubmit = () => {
    // props.navigation.replace('TabNavigator')
    props.navigation.navigate('PermissionInfoScreen')
  }

  const goToLogin = () => {
    props.navigation.replace('LoginScreen')
  }

  return (
    <ScreenLayout
      isHeaderShown={true}
      isHeaderLogo={false}
      hTitle={'Location'}
      headerBackground={Theme.colors.white}
      headerStyle={{ width: '100%', alignSelf: 'center' }}
      showHeaderLine={false}
      customBackground={Theme.colors.secondaryBackground}
      isBackBtn={true}
      backBtnIcnName={'chevron-back-outline'}
      backBtnFunc={() => {
        showBounceAnimation(viewRef);
        props.navigation.goBack();
      }}
      backButton_animationRef={viewRef}>
      <View
        style={{
          flex: 1,
          backgroundColor: Theme.colors.white,
          height: windowHeight - 50,
          width: windowWidth,
        }}>
        <EditTextInputBox placeHolderText="Address *" style={{ marginTop: '10%' }} />
        <View style={{ flexDirection: 'row', paddingTop: 5, paddingLeft: 25 }}>
          <Icon name="locate-outline" color={Theme.colors.primary} size={20} />
          <View style={{ justifyContent: 'center' }}>
            <Text style={{ color: Theme.colors.primary, fontFamily: Theme.FontFamily.medium, fontSize: Theme.sizes.sm, paddingLeft: 5 }}>
              Set Current location using GPS
            </Text>
          </View>
        </View>

        <EditTextInputBox keyboardType={'numeric'} placeHolderText="Zip Code*" style={{ marginTop: '9%' }} />
        <View style={{ marginTop: '9%', flexDirection: 'row', width: '87%', alignSelf: 'center', height: '7%' }}>

          <DropdownPicker
            selectedValue={'Select District*'}
            setDocumentBottomVisible={() => {
              setDocumentBottomVisible(true)
              setSelectedDocumentType("Select District")
            }}
            textStyle={{ colors: '#ABABAB' }}
            textFieldValue="District"
            isHaveTextField={true}
            // selectedValue={selectedDistrict} 
            // setDocumentBottomVisible={setDistrictBottomVisible} 
            style={{ width: '45%', height: '100%' }}
          />
          <DropdownPicker
            selectedValue={'Select State*'}
            setDocumentBottomVisible={() => {
              setDocumentBottomVisible(true)
              setSelectedDocumentType("Select State")
            }}
            textStyle={{ colors: '#ABABAB' }}
            textFieldValue="State"
            isHaveTextField={true}
            // selectedValue={selectedState}
            // setDocumentBottomVisible={setSelectedStateVisible}
            style={{ width: '45%', position: 'absolute', end: 0, height: '100%' }}
          />

          <ModalBottom modalCloseAction={() => { closeDocumentType() }}
            modalHeaderText={selectedDocumentType}
            modalVisible={documentBottomVisible}>
            <View style={{ paddingTop: 40 }}>
              <Text style={{ fontSize: Theme.sizes.h5, lineHeight: 25, textAlign: 'center', fontFamily: Theme.FontFamily.medium, color: 'black' }}>Data should be dynamic, and We will implement in the time of development phase</Text>
            </View>
          </ModalBottom>

        </View>
        <DropdownPicker 
          selectedValue={'Country'}
          setDocumentBottomVisible={() => {
            setDocumentBottomVisible(true)
            setSelectedDocumentType("Country")
          }} 
          // selectedValue={selectedCountry} 
          // setDocumentBottomVisible={setSelectedCountryVisible} 
          style={{ marginTop: '9%' }} />
        <BlackButton style={{ marginTop: '13%' }} buttonText="SAVE" onSubmitPress={() => { clickRegistrationSubmit() }} />

        {/* <View style={{ flexDirection: 'row', paddingTop: 10, alignSelf: 'center', alignItems: 'center' }}>
          <Text style={Styles.newUserText}>Already have account? </Text>
          <TouchableOpacity
            style={Styles.createAccountStyles}
            onPress={() => { props.navigation.replace('LoginScreen') }}
          >
            <Text
              style={Styles.createAccountTextStyles}
            >Login</Text>
          </TouchableOpacity>
        </View> */}
      </View>
    </ScreenLayout>
  )
}

export default LocationScreen;
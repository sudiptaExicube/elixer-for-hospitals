import { View, Text, StyleSheet } from 'react-native'
import React from 'react'
import { Theme } from '../../../constants'

const Style = () => {

  return (
    StyleSheet.create({
 createAccountStyles:{
      fontFamily: Theme.FontFamily.normal,
          //fontStyle: 'normal',
          fontWeight: '500',
          fontSize: 12,
          lineHeight: 15,
          textAlign: 'center',
          color: '#000',
          paddingVertical:5
          
        },
        createAccountTextStyles:{
          fontFamily: Theme.FontFamily.normal,
        //fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 12,
        lineHeight: 15,
        textAlign: 'center',
        color: '#BFA058',
        paddingVertical:5
        },
        text:{
            fontFamily: Theme.FontFamily.normal,
            //fontStyle: 'normal',
            fontWeight: '500',
            fontSize: 13,
            lineHeight: 39,
            letterSpacing: -0.333333,
            textTransform: 'capitalize',
            color: Theme.colors.black,
          },
          newUserText: {
            fontFamily: Theme.FontFamily.normal,
            //fontStyle: 'normal',
            fontWeight: '500',
            fontSize: 12,
            lineHeight: 15,
            textAlign: 'center',
            color: '#000',
          },
          Undergraduate:{
            fontFamily: Theme.FontFamily.normal,
            //fontStyle: 'normal',
            fontWeight: '500',
            fontSize: 15,
            lineHeight: 18,
            alignSelf:'flex-start',
            textAlign: 'center',
            letterSpacing: -0.333333,
            color: Theme.colors.black,
          },
          uploadDocument:{
            fontFamily: Theme.FontFamily.normal,
            //fontStyle: 'normal',
            fontWeight: '500',
            fontSize: 12,
            lineHeight: 39,
            marginVertical:'5%',
            letterSpacing: -0.333333,
            textTransform: 'capitalize',
            color: '#263238',
            marginStart:'7.5%'
          }
    })
  )
}

export default Style
import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import { useTheme } from '../../../constants/ThemeContext';
import { Theme } from '../../../constants';

const Style = () => {
    const {colorTheme} = useTheme;
    return StyleSheet.create({
        createAccountStyles:{
      
            fontFamily: Theme.FontFamily.normal,
          fontStyle: 'normal',
          fontWeight: '500',
          fontSize: 12,
          lineHeight: 15,
          textAlign: 'center',
          color: '#000',
          paddingVertical:5
          
        },
        createAccountTextStyles:{
          fontFamily: Theme.FontFamily.normal,
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 12,
        lineHeight: 15,
        textAlign: 'center',
        color: '#BFA058',
        paddingVertical:5
        },
        newUserText: {
          fontFamily: Theme.FontFamily.normal,
          fontStyle: 'normal',
          fontWeight: '500',
          fontSize: 12,
          lineHeight: 15,
          textAlign: 'center',
          color: '#000',
        },
    })
}

export default Style
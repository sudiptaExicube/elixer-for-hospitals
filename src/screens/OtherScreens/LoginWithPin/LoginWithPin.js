// import { View, Text } from 'react-native'
// import React, { useRef } from 'react'
// import Style from './Style'
// import { ScreenLayout } from '../../../Components'
// import { Theme } from '../../../constants'
// import DoctorIcon from '.././../../assets/images/DoctorIcon.svg'

// const Styles=Style()

// const LoginWithPin = () => {

//   const viewRef = useRef(null);
//   const showBounceAnimation = (value) => {
//     value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
//     value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
//   }
//   return (
//     <ScreenLayout
//       isHeaderShown={true}
//       isHeaderLogo={false}
//       hTitle={""}
//       headerBackground={Theme.colors.white}
//       showHeaderLine={true}
//       customBackground={Theme.colors.secondaryBackground}
//       headerStyle={{width: '94%',alignSelf: 'center'}}
//       isBackBtn={true}
//       backBtnIcnName={"chevron-back-outline"}
//       backBtnFunc={() => {
//         showBounceAnimation(viewRef)
//       }}
//       backButton_animationRef={viewRef}
//     >
//         <View style={Styles.containerBox}>
//             <View style={{borderRadius:79/2,borderWidth: 1,borderColor: '#BFA058',width:79,aspectRatio:1,backgroundColor:'#E7E7E7',alignSelf:'center',alignItems:'center',justifyContent:'flex-end'}}>
//                 <DoctorIcon width={7} height={40} style={{width:1,aspectRatio:1}}/>
//             </View>
//         </View>
//     </ScreenLayout>
//   )
// }

// export default LoginWithPin;

import { View, Text, TouchableOpacity } from 'react-native'
import React, { useRef } from 'react'
import Style from './Style'
import { BlackButton, EditTextInputBox, ScreenLayout } from '../../../Components'
import { Theme } from '../../../constants'
import { Image } from 'react-native'
//import DoctorIcon from '.././../../assets/images/doctor.png'
import Key from '../../../assets/images/Key.svg'

const Styles = Style()

const LoginWithPin = props => {
  /* For Header customization Animation */
  const viewRef = useRef(null);
  const showBounceAnimation = (value) => {
    value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
    value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
  }
  return (
    <ScreenLayout
      isHeaderShown={true}
      isHeaderLogo={false}
      hTitle={""}
      headerBackground={Theme.colors.white}
      showHeaderLine={true}
      customBackground={Theme.colors.secondaryBackground}
      headerStyle={{ width: '94%', alignSelf: 'center' }}
      isBackBtn={true}
      backBtnIcnName={"chevron-back-outline"}
      backBtnFunc={() => {
        showBounceAnimation(viewRef)
        props.navigation.goBack();
      }}
      backButton_animationRef={viewRef}
    >
      <View style={Styles.containerBox}>
        <View style={{ borderRadius: 79 / 2, borderWidth: 1, borderColor: '#BFA058', width: 79, aspectRatio: 1, backgroundColor: '#E7E7E7', alignSelf: 'center', alignItems: 'center', justifyContent: 'flex-end' }}>
          {/* <DoctorIcon  style={{width:1,aspectRatio:1}}/> */}
          <Image source={require("../../../assets/images/doctor.png")} style={{ resizeMode: 'contain', height: 80, width: 200, top: 6 }} />
        </View>
        <Text style={Styles.doctorNameText}>Dr. Doctor’s Name</Text>
        <Text style={Styles.loginText}>Enter your 4 digit pin</Text>
        <EditTextInputBox keyboardType={'numeric'} icon={<Key style={{ marginStart: '4%' }} />} placeHolderText="Enter Pin" style={{ marginTop: '9%' }} />
        
        <BlackButton style={{ marginTop:30 }} buttonText="SUBMIT" onSubmitPress={()=>{props.navigation.replace('TabNavigator')}} />
        
        <View style={{ marginTop: 30, flexDirection: 'row', justifyContent: 'space-around' }}>
          <View style={{ alignSelf: 'flex-start', alignItems: 'center' }}>
            <Text style={{ color: Theme.colors.black }}>Not Dr. Doctor’s Name?</Text>
            <TouchableOpacity style={{ marginTop: 5 }} onPress={() => { props.navigation.replace('LoginScreen') }}>
              <Text style={{ color: '#BFA058', fontFamily: Theme.FontFamily.normal }}>Switch User</Text>
            </TouchableOpacity>
          </View>
          <View style={{ alignSelf: 'flex-end', alignItems: 'center' }}>
            <Text style={{ color: Theme.colors.black }}>Forget Pin ?</Text>
            <TouchableOpacity style={{ marginTop: 5 }} onPress={() => { props.navigation.navigate('ResetPasswordScreen') }}>
              <Text style={{ color: '#BFA058', fontFamily: Theme.FontFamily.normal }}>Reset Pin</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ flexDirection: 'row', marginTop: 40, alignSelf: 'center', alignItems: 'center' }}>
          <Text style={Styles.newUserText}>New user ?  </Text>
          <TouchableOpacity
            style={Styles.createAccountStyles}
            onPress={() => { 
              // props.navigation.replace('PersonalInformation');
              props.navigation.replace("HospitalInfoScreen");
            
            }}
          >
            <Text
              style={Styles.createAccountTextStyles}
            >Create an acoount</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScreenLayout>
  )
}

export default LoginWithPin;
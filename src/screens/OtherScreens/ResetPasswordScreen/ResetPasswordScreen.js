import { View, Text, SafeAreaView, ScrollView } from 'react-native'
import React, { useRef } from 'react'
import { BackHeader, BlackButton, EditTextInputBox, HeaderLine, ScreenLayout } from '../../../Components'
import ResetPasswordLogo from '../../../assets/images/ResetPassword.svg'
import KeyIcon from '../../../assets/images/Key.svg'
import Style from './Style'
import { Theme } from '../../../constants'

const Styles=Style()

const ResetPasswordScreen = props => {

  /* For Header customization Animation */
  const viewRef = useRef(null);
  const showBounceAnimation = (value) => {
    value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
    value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
  }

  return (
    // <SafeAreaView style={{flex:1}}>
    //     <HeaderLine />
    //     <View style={Styles.containerBox}>
    //     <BackHeader headText="Reset Password"/>
    //     <ResetPasswordLogo style={{alignSelf:'center',marginTop:'10%'}}/>
    //     <EditTextInputBox style={{marginTop:'15%'}} icon={<KeyIcon style={{marginStart: '4%'}}/>} placeHolderText="Enter New Pin"/>
    //     <EditTextInputBox style={{marginTop:'5%'}} icon={<KeyIcon style={{marginStart: '4%'}}/>} placeHolderText="Confirm Pin"/>
    //     <BlackButton style={{marginTop:'10%'}} buttonText="SUBMIT"/>
    //     </View>
    // </SafeAreaView>

      <ScreenLayout
      isHeaderShown={true}
      isHeaderLogo={false}
      hTitle={"Reset password"}

      headerBackground={Theme.colors.white}
      headerStyle={{width: '94%',alignSelf: 'center'}}
      headerTextStyle={{fontWeight: '600'}}
      showHeaderLine={true}
      customBackground={Theme.colors.secondaryBackground}
      
      isBackBtn={true}
      backBtnIcnName={"chevron-back-outline"}
      backBtnFunc={() => {
        showBounceAnimation(viewRef)
        props.navigation.goBack();
      }}
      backButton_animationRef={viewRef}
      >
        <View>
        <View style={Styles.containerBox}>
        {/* <BackHeader headText="Reset Password"/> */}
        <ResetPasswordLogo style={{alignSelf:'center',marginTop:'10%'}}/>
        <EditTextInputBox style={{marginTop:'15%',backgroundColor:'#D9D9D924'}} icon={<KeyIcon style={{marginStart: '4%',marginEnd:'1%'}}/>} placeHolderText="Enter New Pin"/>
        <EditTextInputBox style={{marginTop:'5%',backgroundColor:'#D9D9D924'}} icon={<KeyIcon style={{marginStart: '4%',marginEnd:'1%'}}/>} placeHolderText="Confirm Pin"/>
        <BlackButton style={{marginTop:'10%'}} buttonText="SUBMIT" onSubmitPress={() => { props.navigation.replace('LoginScreen') }}/>
        </View>
          
        </View>

      </ScreenLayout>


  )
}

export default ResetPasswordScreen;
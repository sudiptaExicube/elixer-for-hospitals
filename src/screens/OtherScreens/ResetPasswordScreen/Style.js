import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import { Theme } from '../../../constants';
import { windowHeight } from '../../../constants/GlobalStyles';

const Style = () => {
    //   const {colorTheme} = useTheme;
      return StyleSheet.create({
        containerBox: {
            backgroundColor: Theme.colors.white,
            height: windowHeight - 5,
            width: '94%',
            alignSelf: 'center'
          }
        })
    }

export default Style;
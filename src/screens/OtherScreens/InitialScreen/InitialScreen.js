import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	ScrollView, Image, Dimensions
} from 'react-native';
import React, { useEffect, useRef } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { useTheme } from '../../../constants/ThemeContext';
import { GlobalStyles, HelperFunctions, Theme } from '../../../constants';
import { Header, ScreenLayout } from '../../../Components';
import * as Animatable from 'react-native-animatable';


// function InitialScreen({ props,navigation }) {
const InitialScreen = props => {
	// const globalstyles = GlobalStyles();
	const globalstyles = GlobalStyles();
	const onPress = () => HelperFunctions.sampleFunction('its working fine');
	// We're also pulling setScheme here!
	const { setScheme, isDark } = useTheme();
	const toggleScheme = () => {
		isDark ? setScheme('light') : setScheme('dark');
	}


	/* For Header customization Animation */
	const viewRef = useRef(null);
	const showBounceAnimation = (value) => {
		// value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.1, rotate: '0deg' } });
		// value.current.animate({ 0: { scale: 1.1, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */

	/* Use effect start */
	useEffect(() => {
    setTimeout(() => {
			// props.navigation.replace('ResetPasswordScreen');
			props.navigation.replace('IntroLogin');
			// props.navigation.replace('IntroLogin');
    }, 3000);
  }, []);

	return (

		<ScreenLayout
			isHeaderShown={false}
			isHeaderLogo={false}
			hTitle={"Home"}
			customBackground={'rgba(255, 246, 226, 1)'}

			isBackBtn={false}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef)
			}}
			backButton_animationRef={viewRef}
		>
			{/* <ScrollView showsVerticalScrollIndicator={false}> */}
			<View style={{ flexDirection: 'column', justifyContent: 'center', height: Dimensions.get('window').height }}>
				<View style={{ flexDirection: 'row', justifyContent: 'center' }}>
					<Animatable.Image 
						style={{ height: 230, width: 230 }}
						source={require('../../../assets/images/logo.png')}
						// animation={'zoomInUp'}
						animation={'pulse'}
						easing="ease-out" 
						iterationCount="infinite"
						>      
					</Animatable.Image>
					

				</View>
				<View style={{ flexDirection: 'row', justifyContent: 'center' }}>
					<Animatable.Image 
						style={{ height: 230, width: 230, resizeMode:'contain' }}
						source={require('../../../assets/images/app_name.png')}
						animation={'zoomInUp'}
						// animation={'pulse'}
						easing="ease-out" 
						// iterationCount="infinite"
						>      
					</Animatable.Image>
					

				</View>

			</View>
			{/* </ScrollView> */}
		</ScreenLayout>
	)
}

export default InitialScreen;
import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	Pressable
} from 'react-native';
import React, { useRef, useState } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../constants';
import { BlackButton, ScreenLayout } from '../../Components';
// import * as Animatable from 'react-native-animatable';
// import Permission from '../../assets/images/permission.svg';
import Style from './Style';
// import { Rating, AirbnbRating } from 'react-native-ratings';
// import { TextInput } from 'react-native';
import NotificationPermission from '../../assets/images/notification_permission.svg'
import MapPermission from '../../assets/images/map_permission.svg'

const PermissionScreen= props => {
	const Styles = Style()
	const onPress = () => HelperFunctions.sampleFunction('its working fine');

	const [locationToggleValue, setLocationToggleValue] = useState(false);
	const [notifiationToggleValue, setNotifiationToggleValue] = useState(false);

	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */

	const reviewButtonClick = () => {

	}

	const changeLocationValue = () => {
		setLocationToggleValue(!locationToggleValue);
	}

	const changeNotificationValue = () => {
		setNotifiationToggleValue(!notifiationToggleValue);
	}

	


	return (

		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={'Permission'}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={Theme.colors.primary}
			showHeaderLine={false}
			customBackground={Theme.colors.primary}
			isBackBtn={true}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				props.navigation.goBack();

			}}
			backButton_animationRef={viewRef}
		>
			<View style={Styles.mainView}>

				<View style={{paddingTop:'45%'}}>
					<View style={{ flexDirection: 'column', justifyContent: 'center', width: '95%', backgroundColor: Theme.colors.white, alignSelf: 'center', height: 150,borderRadius:15,marginBottom:15}}>
						<View style={{ flexDirection: 'row'}}>
							<View style={{flexDirection:'column',justifyContent:'center',paddingHorizontal:15}}>
								<MapPermission height={50} width={50}  />
							</View>
							<View style={{flex:1,flexDirection:'column',justifyContent:'space-evenly'}}>
								<Text style={{fontFamily:Theme.FontFamily.medium,fontWeight: '600',fontSize:Theme.sizes.h5,color:Theme.colors.black,paddingBottom:10}}>Share Your Location</Text>
								<Text style={{fontFamily:Theme.FontFamily.medium,fontWeight:'600',fontSize:Theme.sizes.h7,color:Theme.colors.grey}}>Allow access to your current location to get update based on your location</Text>
							</View>
							<View style={{flexDirection:'column',justifyContent:'center',paddingHorizontal:15}}>
								<TouchableOpacity 
									onPress={()=>{changeLocationValue()}}
									style={{height:27,width:54,backgroundColor:'white',borderWidth:1,borderColor:Theme.colors.grey,flexDirection:'row'}}>
									<View style={{flex:1,backgroundColor:locationToggleValue == false ? Theme.colors.white : Theme.colors.primary,margin:0.5}}></View>
									<View style={{flex:1,backgroundColor:locationToggleValue == true ? Theme.colors.white : Theme.colors.primary,margin:0.5}}></View>
								</TouchableOpacity>
							</View>
						</View>
					</View>


					<View style={{ flexDirection: 'column', justifyContent: 'center', width: '95%', backgroundColor: Theme.colors.white, alignSelf: 'center', height: 150,borderRadius:15 }}>
						<View style={{ flexDirection: 'row'}}>
							<View style={{flexDirection:'column',justifyContent:'center',paddingHorizontal:15}}>
								<NotificationPermission height={50} width={50}  />
							</View>
							<View style={{flex:1,flexDirection:'column',justifyContent:'space-evenly'}}>
								<Text style={{fontFamily:Theme.FontFamily.medium,fontWeight: '600',fontSize:Theme.sizes.h5,color:Theme.colors.black,paddingBottom:10}}>Allow Notification Access</Text>
								<Text style={{fontFamily:Theme.FontFamily.medium,fontWeight:'600',fontSize:Theme.sizes.h7,color:Theme.colors.grey}}>Give notification access to get
latest updated on the app</Text>
							</View>
							<View style={{flexDirection:'column',justifyContent:'center',paddingHorizontal:15}}>
								<TouchableOpacity 
									onPress={()=>{changeNotificationValue()}}
									style={{height:27,width:54,backgroundColor:'white',borderWidth:1,borderColor:Theme.colors.grey,flexDirection:'row'}}>
									<View style={{flex:1,backgroundColor:notifiationToggleValue == false ? Theme.colors.white : Theme.colors.primary,margin:0.5}}></View>
									<View style={{flex:1,backgroundColor:notifiationToggleValue == true ? Theme.colors.white : Theme.colors.primary,margin:0.5}}></View>
								</TouchableOpacity>
							</View>
						</View>
					</View>
				</View>

			</View>


		</ScreenLayout>
	)
}

export default PermissionScreen;
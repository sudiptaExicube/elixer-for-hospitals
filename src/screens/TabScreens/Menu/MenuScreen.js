import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	Pressable,
	Platform
} from 'react-native';
import React, { useRef, useState } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../../constants';
import { BlackButton, ScreenLayout } from '../../../Components';
import AboutUs from '../../../assets/images/about_us.svg';
import { FlatList } from 'react-native';
import { Image } from 'react-native';
import CustomMenuIcon from './MenuArray';

import Permission from '../../../assets/images/permission.svg';
import SignOut from '../../../assets/images/signOut.svg';
import HelpNsupport from '../../../assets/images/helpNsupport.svg';
import Menu_profile from '../../../assets/images/menu_profile.svg';
import SignoutIcon from '../../../assets/images/signout_icon.svg';
import CloseSvgIcon from '../../../assets/images/close_icon.svg';
import Rate_us from '../../../assets/images/rate_us.svg';
import Style from './Style';
import { Modal } from 'react-native';


function MenuScreen(props) {
	const Styles = Style()
	const onPress = () => HelperFunctions.sampleFunction('its working fine');

	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */

	/* === Modal === */
	const [modalVisible, setModalVisible] = useState(false);

	const menuData = [
		{ name: 'About us', iconComponent: AboutUs },
		{ name: 'Permission', iconComponent: Permission },
		{ name: 'Rate app', iconComponent: Rate_us },
		{ name: 'Profile', iconComponent: Menu_profile },
		{ name: 'Help and support', iconComponent: HelpNsupport },
		{ name: 'Sign out', iconComponent: SignOut },
	]

	/* == Menu item click function ==*/
	const menuItemClick = (item) => {
		if (item.name == 'Sign out') {
			setModalVisible(true)
		} else if (item.name == 'Rate app') {
			props.navigation.navigate('RatingScreen')
		} else if (item.name == 'Permission') {
			props.navigation.navigate('PermissionScreen')
			// props.navigation.navigate('PermissionInfoScreen')

		} else if (item.name == 'About us') {
			props.navigation.navigate('AboutusScreen')
		} else if (item.name == 'Help and support') {
			props.navigation.navigate('HelpScreen')
		} else if (item.name == 'Profile') {
			props.navigation.navigate('ProfileScreen')
		}






	}

	/* == Render menu item return function ==*/
	const renderItem = ({ item }) => (
		<TouchableOpacity style={Styles.mainTouchableopacity} onPress={() => { menuItemClick(item) }}>
			<View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', paddingHorizontal: 15 }}>
				<View style={{ flexDirection: 'column', justifyContent: 'center' }}>
					<CustomMenuIcon svgName={item?.iconComponent} height={30} width={30} />
				</View>
				<View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', paddingLeft: 15 }}>
					<Text style={Styles.itemText}>{item?.name}</Text>
				</View>
			</View>
		</TouchableOpacity>
	)


	const clickLoginSubmit = () => {
		setModalVisible(false)
		setTimeout(() => {
		props.navigation.replace('LoginScreen');
		}, 500);
	}



	return (

		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={"Menu"}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={Theme.colors.primary}
			showHeaderLine={false}
			customBackground={Theme.colors.primary}
			isBackBtn={false}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef)
			}}
			backButton_animationRef={viewRef}
		// showScrollView={true}
		>
			<View style={Styles.mainView}>
				<View style={{ height: 30 }}></View>
				<FlatList
					data={menuData}
					renderItem={renderItem}
					// keyExtractor={(item, index) => item.id}
					keyExtractor={(item, index) => index}
					showsVerticalScrollIndicator={false}
					showsHorizontalScrollIndicator={false}
				// style={{paddingTop:30}}
				/>

				<View style={Styles.centeredView}>
					<Modal
						animationType="fade"
						// style={{backgroundColor: 'black'}}
						transparent={true}
						backdropOpacity={0.4}
						activeOpacity={0.6}
						visible={modalVisible}
						onRequestClose={() => {
							Alert.alert('Modal has been closed.');
							setModalVisible(!modalVisible);
						}}>
						<View style={[Styles.centeredView, { backgroundColor: 'rgba(0, 0, 0, 0.8)' }]}>
							<View style={Styles.modalView}>
								<View style={{ flex: 1 }}>
									<View style={{ flexDirection: 'row', justifyContent: 'flex-end', width: '100%' }}>
										<TouchableOpacity onPress={() => { setModalVisible(false) }}>
											<CustomMenuIcon svgName={CloseSvgIcon} height={17} width={17} />
										</TouchableOpacity>
									</View>
									<View style={{ flex: 3, flexDirection: 'row', justifyContent: 'center' }}>
										<CustomMenuIcon svgName={SignoutIcon} height={160} width={160} />
									</View>
									<View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-end' }}>
										<View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
											<View style={{ flex: 1 }}>
												<BlackButton style={{ height: 43 }} buttonText="CANCEL"
													onSubmitPress={() => { clickLoginSubmit() }}
													textCustomStyle={{ fontWeight: '600', fontFamily: Theme.FontFamily.bold, fontSize: Theme.sizes.sm }}
												/>
											</View>
											<View style={{ flex: 1 }}>
												<BlackButton style={{ height: 43, backgroundColor: Theme.colors.primary }}
													textColor={Theme.colors.black}
													textCustomStyle={{ fontWeight: '600', fontFamily: Theme.FontFamily.bold, fontSize: Theme.sizes.sm }}
													buttonText="SIGN OUT" onSubmitPress={() => { clickLoginSubmit() }} />
											</View>
										</View>
									</View>
								</View>

							</View>

						</View>

					</Modal>
				</View>
				{Platform.OS == 'ios' ?
					<View style={{ paddingBottom: '8%' }}></View>
					: null}

			</View>


		</ScreenLayout>
	)
}

export default MenuScreen
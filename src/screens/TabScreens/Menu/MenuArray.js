import React from 'react';
// import AboutUs from '../../../assets/images/about_us.svg'
// import NewAboutUs from '../../../assets/images/about_us.svg'
// import OldAboutUs from '../../../assets/images/about_us.svg'

// const CustomSvgIconName = {
//     AboutUs: AboutUs,
//     NewAboutUs: NewAboutUs,
//     OldAboutUs: OldAboutUs,
// }

export default function CustomMenuIcon(props) {
  const SvgIconCustom = props.svgName;
  return <SvgIconCustom width={props.width ? props.width : 25} height={props.height ? props.height : 25} />;
}
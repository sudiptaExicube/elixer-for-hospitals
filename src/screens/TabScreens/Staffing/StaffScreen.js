import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	Pressable,
	Platform,
	TouchableWithoutFeedback
} from 'react-native';
import React, { useRef, useState } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../../constants';
import { BlackButton, ScreenLayout, EditTextInputBox } from '../../../Components';
import Style from './Style';
import { ScrollView } from 'react-native';
import { FlatList } from 'react-native';
import { Image } from 'react-native';


// function SearchScreen({ navigation }) {
const StaffScreen = props => {
	const customArrowIcon = require('../../../assets/images/custom_right_arrow.png')
	const Styles = Style()
	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */

	const [selectedType, setSelectedType] = useState(null)

	const [selectedIndex, setSelectedIndex] = useState('Doctor');
	const activeStyle = { borderColor: Theme.colors.black, backgroundColor: Theme.colors.black }
	const defaultStyle = { borderColor: Theme.colors.grey }
	const activeTextStyle = { color: Theme.colors.white }
	const defaultTextStyle = { color: Theme.colors.grey }

	const teamDetails = [
		{ name: '2023', designation: 'Developer', imageUrl: require('../../../assets/images/hospital_img1.png'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500 csdkb cdsc b dscbdskc bdskjcbd skjcbdskj cbsdkbdskcb" },
		{ name: '2022', designation: 'Manager', imageUrl: require('../../../assets/images/hospital_img2.png'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500" },
		{ name: '2021', designation: 'Product lead', imageUrl: require('../../../assets/images/hospital_img3.png'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500" },
		{ name: '2020', designation: 'Developer', imageUrl: require('../../../assets/images/hospital_img1.png'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500 csdkb cdsc b dscbdskc bdskjcbd skjcbdskj cbsdkbdskcb" }
	]
	const nurseDetails = [
		{ name: '2023', designation: 'Developer', imageUrl: require('../../../assets/images/hospital_img1.png'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500 csdkb cdsc b dscbdskc bdskjcbd skjcbdskj cbsdkbdskcb" },
		{ name: '2022', designation: 'Manager', imageUrl: require('../../../assets/images/hospital_img2.png'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500" },
		{ name: '2021', designation: 'Product lead', imageUrl: require('../../../assets/images/hospital_img3.png'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500" },
		{ name: '2020', designation: 'Developer', imageUrl: require('../../../assets/images/hospital_img1.png'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500 csdkb cdsc b dscbdskc bdskjcbd skjcbdskj cbsdkbdskcb" }
	]

	/* == Render menu item return function ==*/
	const renderItem = ({ item }) => (
		<TouchableWithoutFeedback onPress={()=>{
			props.navigation.navigate('StaffDetailsScreen')
		}}>
			<View>
			<View style={{paddingBottom:10,paddingTop:10}}>
				<Text style={{fontFamily:Theme.FontFamily.medium,color:Theme.colors.black}}>Payment of {item.name}</Text>
			</View>
			<View style={{backgroundColor:'white',width:'100%',
						// shadowColor: '#000',
						// shadowOffset: { width: 0, height: 1 },
						// shadowOpacity: 0.8,
						// shadowRadius: 1, 
						// elevation:2,
						borderWidth:1,
						borderRadius:5,
						borderColor:Theme.colors.background_shade1,
						flexDirection:'row',justifyContent:'space-between',padding:10,paddingHorizontal:15,marginBottom:15
						}}
				>
					<View style={{flex:1,height:50,justifyContent:'center',borderRightWidth:1,borderRightColor:'black',justifyContent:'space-evenly'}}>
						<Text style={{fontSize:10,textAlign:'center',fontFamily:Theme.FontFamily.medium,color:Theme.colors.grey}}>
							{/* Doctor / Nurse */}
							{selectedIndex}
						</Text>
						<Text style={{fontSize:14,textAlign:'center',fontFamily:Theme.FontFamily.bold,color:'black'}}>18</Text>
					</View>
					<View style={{flex:1.2,height:50,justifyContent:'center',justifyContent:'space-evenly'}}>
						<Text style={{fontSize:10,textAlign:'center',fontFamily:Theme.FontFamily.medium,color:Theme.colors.grey}}>Total hours booked </Text>
						<Text style={{fontSize:14,textAlign:'center',fontFamily:Theme.FontFamily.bold,color:'black'}}>240</Text>
					</View>
					<View style={{flex:1.2,height:50,justifyContent:'center',justifyContent:'space-evenly'}}>
						<Text style={{fontSize:10,textAlign:'center',fontFamily:Theme.FontFamily.medium,color:Theme.colors.grey}}>Total Earned</Text>
						<View style={{flexDirection:'row',alignSelf:'center'}}>
							<Image source={require('../../../assets/images/rupee-indian.png')} style={{ height: 12, width: 12, resizeMode: 'contain',justifyContent:'center',alignSelf:'center'}} />
							<Text style={{fontSize:14,textAlign:'center',fontFamily:Theme.FontFamily.bold,color:Theme.colors.primary}}>115,000</Text>
						</View>
					</View>

				</View>
				</View>
		</TouchableWithoutFeedback>		
			)



	return (

		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={'Staffing'}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={Theme.colors.primary}
			showHeaderLine={false}
			// customBackground={Theme.colors.white}
			customBackground={'#F8FBFF'}
			isBackBtn={false}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				props.navigation.goBack();

			}}
			backButton_animationRef={viewRef}
			showScrollView={true}
		>
			<View style={Styles.mainView}>
				<View style={{ height: 80, width: '100%', backgroundColor: Theme.colors.primary }}>
					<View style={{ width: '93%', alignSelf: 'center', backgroundColor: 'white', height: 100 }}></View>
				</View>
				<View style={{ position: 'absolute', top: 10, height: WindowData.customBody_height-55, width: '93%', backgroundColor: Theme.colors.white, alignSelf: 'center', paddingVertical: 10 }}>
					
					<View style={{ flexDirection: 'row', paddingBottom: 10,paddingHorizontal:10 }}>
						<TouchableOpacity
							onPress={() => { setSelectedIndex("Doctor") }}
							style={[selectedIndex == 'Doctor' ? activeStyle : defaultStyle, { borderWidth: 1, borderRadius: 6, flex: 1, justifyContent: 'center', alignItems: 'center', paddingVertical: 10, marginRight: 5, paddingHorizontal: 5 }]}>
							<View style={{ flexDirection: 'column', justifyContent: 'center' }}>
								<Text style={[selectedIndex == 'Doctor' ? activeTextStyle : defaultTextStyle, { fontSize: Theme.sizes.h7, letterSpacing: 0.6, textTransform: 'capitalize' }]}>Doctor</Text>
							</View>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() => { setSelectedIndex("Nurse") }}
							style={[selectedIndex == 'Nurse' ? activeStyle : defaultStyle, { borderWidth: 1, borderRadius: 6, flex: 1, justifyContent: 'center', alignItems: 'center', paddingVertical: 10, marginRight: 5, paddingHorizontal: 5 }]}>
							<View style={{ flexDirection: 'column', justifyContent: 'center' }}>
								<Text style={[selectedIndex == 'Nurse' ? activeTextStyle : defaultTextStyle, { fontSize: Theme.sizes.h7, letterSpacing: 0.6, textTransform: 'capitalize' }]}>Nurse</Text>
							</View>
						</TouchableOpacity>
					</View>

					<ScrollView style={{ flex: 1 }}
						showsVerticalScrollIndicator={false}
						showsHorizontalScrollIndicator={false}
					>
						<View style={{paddingHorizontal:15}}>
							<FlatList
								horizontal={false}
								data={ selectedIndex == 'Doctor' ? teamDetails : nurseDetails}
								renderItem={renderItem}
								// keyExtractor={(item, index) => item.id}
								keyExtractor={(item, index) => index}
								showsVerticalScrollIndicator={false}
								showsHorizontalScrollIndicator={false}
							/>
						</View>
					</ScrollView>




					{Platform.OS == 'ios'?
						<View style={{paddingBottom:'8%'}}></View>
					:null}
				</View>


			</View>


		</ScreenLayout>
	)
}

export default StaffScreen;
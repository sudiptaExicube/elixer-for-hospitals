import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	ScrollView, Image, Platform
} from 'react-native';
import React, { useRef, useState } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
// import { useTheme } from '../../../constants/ThemeContext';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../../constants';
import { BlackButton, Header, ScreenLayout } from '../../../Components';
import { FlatList } from 'react-native';
import Style from './Style';
// import { Theme } from '../../../constants';
// import CustomLocation from '../../../assets/images/custom_location.svg'
import FilterIcon from '../../../assets/images/filter_icon.svg'

function HomeScreen(props) {
	// const globalstyles = GlobalStyles();
	const Styles = Style()
	const globalstyles = GlobalStyles();
	const onPress = () => HelperFunctions.sampleFunction('its working fine');

	const customLocationIcon = require('../../../assets/images/custom_location.png')

	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */

	const teamDetails = [
		{ name: 'Orthopedic', staff_type:'Doctor', date:'20-06-22', time:'12am - 8am',type: 'Emergency', designation: 'Developer', imageUrl: require('../../../assets/images/doctor_profile.png') },
		{ name: 'Cardiologists', staff_type:'Nurse', date:'20-06-22', time:'12am - 8am',type: 'Consultation', mode: 'ONLINE', designation: 'Manager', imageUrl: require('../../../assets/images/nurse_image.png') },
		{ name: 'audiologists', staff_type:'Doctor', date:'20-06-22', time:'12am - 8am',type: 'Consultation', mode: 'OFFLINE', designation: 'Product lead', imageUrl: require('../../../assets/images/doctor_profile.png') },
		{ name: 'dentists', staff_type:'Nurse', date:'20-06-22', time:'12am - 8am',type: 'Emergency', designation: 'Developer', imageUrl: require('../../../assets/images/nurse_image.png') },
		{ name: 'pediatricians', staff_type:'Nurse', date:'20-06-22', time:'12am - 8am',type: 'Emergency', designation: 'Manager', imageUrl: require('../../../assets/images/nurse_image.png') },
		{ name: 'psychiatrists', staff_type:'Doctor', date:'20-06-22', time:'12am - 8am',type: 'ICU', designation: 'Product lead', imageUrl: require('../../../assets/images/doctor_profile.png') },
		{ name: 'veterinarians', staff_type:'Nurse', date:'20-06-22', time:'12am - 8am',type: 'ICU', designation: 'Developer', imageUrl: require('../../../assets/images/nurse_image.png') },
		{ name: 'endocrinologists', staff_type:'Doctor', date:'20-06-22', time:'12am - 8am',type: 'Emergency', designation: 'Manager', imageUrl: require('../../../assets/images/doctor_profile.png') },
		{ name: 'neurologists', staff_type:'Nurse', date:'20-06-22', time:'12am - 8am',type: 'Emergency', designation: 'Product lead', imageUrl: require('../../../assets/images/nurse_image.png') },
	]

	/* == Render menu item return function ==*/
	const renderItem = ({ item }) => (
		<TouchableOpacity style={{}}
			onPress={()=>{props.navigation.navigate('AppointmentDetailsScreen',{item: item})}}
		>
			<View style={{paddingHorizontal:10,backgroundColor:'#FAFAFA',marginBottom:10, flex: 1, flexDirection: 'row', paddingVertical: 10, width: '100%', height: 100 }}>

				<View style={{ justifyContent: 'center', flexDirection: 'column' }}>
					<View style={{ height: 75, width: 75, borderRadius: 75, justifyContent: 'center', flexDirection: 'column' }}>
						<Image source={item?.imageUrl} style={{ height: 74, width: 74, borderRadius: 74, resizeMode: 'cover', borderColor: Theme.colors.primary, borderWidth: 1 }} />
					</View>
				</View>
				<View style={{ flex: 1, justifyContent: 'center', flexDirection: 'column' }}>
					<View style={{ paddingHorizontal: 10, justifyContent: 'center', flexDirection: 'column' }}>
						<Text style={[Styles.itemText, { fontFamily: Theme.FontFamily.bold, paddingBottom: 3, fontSize: Theme.sizes.h5, flexWrap: 'wrap', color: Theme.colors.black, letterSpacing: 0.5,
							textShadowColor: 'rgba(0, 0, 0, 0.25)',
							textShadowOffset: { width: 0, height: 4 },
							textShadowRadius: 4,
							textTransform:'capitalize'
						}]}>{item?.name}</Text>
						<View style={{ flexDirection: 'row',paddingTop:5 }}>
								<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.primary }]}>Specialist: </Text>
								<Text style={[Styles.itemText, {
									paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.black,
									textShadowColor: 'rgba(0, 0, 0, 0.25)',
									textShadowOffset: { width: 0, height: 4 },
									textShadowRadius: 4,
								}]}>{item?.mode} {item?.type}</Text>
						</View>
						<View style={{ flexDirection: 'row',paddingTop:5 }}>
								<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.primary }]}>Staff: </Text>
								<Text style={[Styles.itemText, {
									paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.black,
									textShadowColor: 'rgba(0, 0, 0, 0.25)',
									textShadowOffset: { width: 0, height: 4 },
									textShadowRadius: 4,
								}]}>{item?.staff_type}</Text>
						</View>
						<View style={{ flexDirection: 'row',justifyContent:'space-between',paddingTop:5 }}>
							<View style={{ flexDirection: 'row' }}>
									<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.primary }]}>Date: </Text>
									<Text style={[Styles.itemText, {
										paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.black,
									}]}>{item?.date}</Text>
								</View>
								<View style={{ flexDirection: 'row' }}>
									<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.primary }]}>Time: </Text>
									<Text style={[Styles.itemText, {
										paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.black,
									}]}>{item?.time}</Text>
								</View>
						</View>
					</View>
				</View>
				{/* <View style={{ justifyContent: 'center', flexDirection: 'column', paddingHorizontal: 5 }}>
					<BlackButton buttonText={'Book now'} onSubmitPress={() => {
						props.navigation.navigate('HospitalDetailsScreen', { item: item })
						console.log(item)
					}} style={{ height: 30, width: 70 }} textColor={Theme.colors.primary} textCustomStyle={{ fontSize: 10 }} />
				</View> */}

			</View>

		</TouchableOpacity>
	)

	const renderItem2 = ({ item }) => (
		<View style={{ paddingRight: 15 }}>
			<View style={{ height: 70, width: 70, borderRadius: 10 }}>
				<Image source={item?.imageUrl} style={{ height: 70, width: 70, borderRadius: 10, resizeMode: 'cover', borderColor: Theme.colors.primary, borderWidth: 1 }} />
			</View>
			<Text numberOfLines={2} style={{ width: 70, textAlign: 'center', fontSize: 10, paddingTop: 5, fontFamily: Theme.FontFamily.normal }}>Lorem Ipsum</Text>
		</View>
	)


	const [selectedIndex, setSelectedIndex] = useState('icu');
	const activeStyle = { borderColor: Theme.colors.black, backgroundColor: Theme.colors.black }
	const defaultStyle = { borderColor: Theme.colors.grey }
	const activeTextStyle = { color: Theme.colors.white }
	const defaultTextStyle = { color: Theme.colors.grey }


	return (


		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={'Home'}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={Theme.colors.primary}
			showHeaderLine={false}
			// customBackground={Theme.colors.white}
			customBackground={'#F8FBFF'}
			isBackBtn={false}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				props.navigation.goBack();

			}}
			backButton_animationRef={viewRef}
			showScrollView={true}
			// icon1Name="home"
			svgIconPath={<FilterIcon height={27} width={27} />}
			icon1Click={() => {props.navigation.navigate('FilterScreen') }}
		>
			<View style={Styles.mainView}>
				<View style={{ height: 106, width: '100%', backgroundColor: Theme.colors.primary }}>
					<View style={{ width: '95%', alignSelf: 'center', backgroundColor: 'white', height: 100 }}></View>
				</View>

				<View style={{ position: 'absolute', top: 10, height: WindowData.customBody_height - 60, width: '95%', backgroundColor: Theme.colors.white, alignSelf: 'center', paddingVertical: 10 }}>



				<View style={{ flexDirection: 'row', paddingBottom: 10,paddingHorizontal:15 }}>
						<TouchableOpacity
							onPress={() => { setSelectedIndex("icu") }}
							style={[selectedIndex == 'icu' ? activeStyle : defaultStyle, { borderWidth: 1, borderRadius: 6, flex: 1, justifyContent: 'center', alignItems: 'center', paddingVertical: 5, marginRight: 5, paddingHorizontal: 5 }]}>
							<View style={{ flexDirection: 'column', justifyContent: 'center' }}>
								<Text style={[selectedIndex == 'icu' ? activeTextStyle : defaultTextStyle, { fontSize: Theme.sizes.h7, letterSpacing: 0.6, textTransform: 'capitalize' }]}>In progress</Text>
							</View>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() => { setSelectedIndex("emergency") }}
							style={[selectedIndex == 'emergency' ? activeStyle : defaultStyle, { borderWidth: 1, borderRadius: 6, flex: 1, justifyContent: 'center', alignItems: 'center', paddingVertical: 5, marginRight: 5, paddingHorizontal: 5 }]}>
							<View style={{ flexDirection: 'column', justifyContent: 'center' }}>
								<Text style={[selectedIndex == 'emergency' ? activeTextStyle : defaultTextStyle, { fontSize: Theme.sizes.h7, letterSpacing: 0.6, textTransform: 'capitalize' }]}>Pending</Text>
							</View>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() => { setSelectedIndex("consultation") }}
							style={[selectedIndex == 'consultation' ? activeStyle : defaultStyle, { borderWidth: 1, borderRadius: 6, flex: 1, justifyContent: 'center', alignItems: 'center', paddingVertical: 5, paddingHorizontal: 5 }]}>
							<View style={{ flexDirection: 'column', justifyContent: 'center' }}>
								<Text style={[selectedIndex == 'consultation' ? activeTextStyle : defaultTextStyle, { fontSize: Theme.sizes.h7, letterSpacing: 0.6, textTransform: 'capitalize' }]}>Complete</Text>
							</View>
						</TouchableOpacity>
						{/* <View style={{flex:2}}></View> */}
					</View>





					{/* <View style={{ paddingBottom: 10, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#F1F1F1', marginBottom: 20, paddingHorizontal: 15 }}>
						<View style={{ height: 70, width: 70, borderRadius: 70, alignSelf: 'center', justifyContent: 'center', flexDirection: 'row' }}>
							<Image source={require('../../../assets/images/doctor_profile.png')} style={{ height: 70, width: 70, borderRadius: 70, resizeMode: 'contain', borderColor: Theme.colors.primary, borderWidth: 2, }} />
							<View style={{ position: 'absolute', bottom: 0, right: -3 }}>
								<View style={{ height: 25, width: 30, borderRadius: 5, backgroundColor: 'white', flexDirection: 'column', justifyContent: 'center', alignSelf: 'center' }}>
									<Image source={require('../../../assets/images/cameraIcon.png')} style={{ height: 20, width: 20, resizeMode: 'cover', alignSelf: 'center' }} />
								</View>
							</View>
						</View>
						<View style={{ flex: 1, paddingLeft: 15,justifyContent:'center' }}>
							<Text numberOfLines={2} style={{ fontSize: Theme.sizes.h6, paddingTop: 5, color: Theme.colors.black, fontFamily: Theme.FontFamily.bold }}>
								Hello Doctor {"<Firth name>"}
							</Text>
						</View>
					</View> */}

					{/* <Text
						style={{paddingHorizontal:15,fontFamily: Theme.FontFamily.medium, fontSize: Theme.sizes.h5, color: Theme.colors.black, paddingBottom: 10 }}
					>
						All Bookings for Today
					</Text> */}

					<ScrollView style={{ flex: 1,paddingHorizontal:5,marginHorizontal:10 }}
						showsVerticalScrollIndicator={false}
						showsHorizontalScrollIndicator={false}
					>
						<View>
							<FlatList
								horizontal={false}
								data={teamDetails}
								renderItem={renderItem}
								// keyExtractor={(item, index) => item.id}
								keyExtractor={(item, index) => index}
								showsVerticalScrollIndicator={false}
								showsHorizontalScrollIndicator={false}
							/>
						</View>
					</ScrollView>
					
					<View style={{position:'absolute',bottom:Platform.OS == 'ios' ? '8%' : '1%',justifyContent:'center',alignSelf:'center'}}>
							<TouchableOpacity 
								onPress={()=>{
									// props.navigation.navigate('FilterScreen')
									props.navigation.navigate('CreateTaskScreen')
								}}
								style={{
									shadowColor: Theme.colors.black,
									shadowOffset: {width: 0, height: 2},
									shadowOpacity: 0.3,
									shadowRadius: 1,
									paddingHorizontal:20,paddingVertical:10, borderRadius:100, backgroundColor:Theme.colors.primary,justifyContent:'center',alignItems:'center'}}>
								{/* <FilterIcon height={27} width={27} /> */}
								<Text style={{fontFamily:Theme.FontFamily.bold,letterSpacing:0.5,textTransform:'capitalize'}}>Create new</Text>
							</TouchableOpacity>
					</View>
					
					{Platform.OS == 'ios' ?
						<View style={{ paddingBottom: '8%' }}></View>
						: null}

				</View>


			

			</View>
			


		</ScreenLayout>


	)
}

export default HomeScreen
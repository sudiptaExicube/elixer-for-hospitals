import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	ScrollView, Image, Platform
} from 'react-native';
import React, { useRef, useState } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
// import { useTheme } from '../../../constants/ThemeContext';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../../constants';
import { Header, ScreenLayout } from '../../../Components';
import { FlatList } from 'react-native';
import Style from './Style';
// import { Theme } from '../../../constants';
import CloseIcon from '../../../assets/images/close_icon_red.svg'
import DisableIcon from '../../../assets/images/disable_icon.svg'
import SimpleIcon from 'react-native-vector-icons/SimpleLineIcons'
import FilterIcon from '../../../assets/images/filter_icon.svg'

function ScheduleScreen(props) {
	// const globalstyles = GlobalStyles();
	const Styles = Style()
	const globalstyles = GlobalStyles();
	const onPress = () => HelperFunctions.sampleFunction('its working fine');

	const [selectedIndex, setSelectedIndex] = useState('icu');

	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */

	const icuDetails = [
		{ name: 'Pradip Mondal', type: 'Emergency'},
		{ name: 'Sudipta Mukherjee', type: 'Consultation', mode: 'ONLINE'},
		{ name: 'Pramit Paul', type: 'Consultation', mode: 'OFFLINE'},
		{ name: 'Sudipta Mukherjee', type: 'Consultation', mode: 'ONLINE'},
		{ name: 'Pramit Paul', type: 'Product lead'},
		{ name: 'Pradip Mondal', type: 'Developer'},
		{ name: 'Sudipta Mukherjee', type: 'ICU'},
		{ name: 'Pramit Paul', type: 'Emergency'},
		{ name: 'Sudipta Mukherjee', type: 'ICU'},
		{ name: 'Pramit Paul', type: 'ICU'}
	]

	const emergencyDetails = [
		{ name: 'Pradip Mondal', type: 'Emergency'},
		{ name: 'Sudipta Mukherjee', type: 'Consultation', mode: 'ONLINE'},
		{ name: 'Pramit Paul', type: 'Consultation', mode: 'OFFLINE'},
	]

	const consultationDetails = [
		{ name: 'Pradip Mondal', type: 'Emergency'},
		{ name: 'Sudipta Mukherjee', type: 'Consultation'},
		{ name: 'Pramit Paul', type: 'Consultation', mode: 'OFFLINE'},
	]

	/* == Render menu item return function ==*/
	const renderItem = ({ item }) => (
		<TouchableOpacity style={{}} onPress={() => {
			// props.navigation.navigate('AppointmentDetailsScreen')
			//props.navigation.navigate('AppointmentDetailsScreen', { item: item })
		}}>
			{/* <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', paddingVertical: 10, width: '100%', height: 100, marginBottom: 20 }}>
				<View style={{ height: 95, width: 95, borderRadius: 5 }}>
					<Image source={item?.imageUrl} style={{ height: 94, width: 94, borderRadius: 5, resizeMode: 'cover', borderColor: Theme.colors.primary, borderWidth: 1 }} />
				</View>
				<View style={{ paddingHorizontal: 10 }}>
					<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.h6, flexWrap: 'wrap', color: Theme.colors.primary, fontFamily: Theme.FontFamily.bold }]}>Hospital Name</Text>

					<View style={{ flexDirection: 'row' }}>
						<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.primary }]}>
							Description:
						</Text>
						<Text style={[Styles.itemText, {
							paddingBottom: 3, fontSize: Theme.sizes.sm,
							textShadowColor: 'rgba(0, 0, 0, 0.25)',
							textShadowOffset: { width: 0, height: 4 },
							textShadowRadius: 4,
						}]}> {item?.mode} {item?.type}</Text>
					</View>

					<View style={{ flexDirection: 'row' }}>
						<View style={{ flexDirection: 'row' }}>
							<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.primary }]}>Date: </Text>
							<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm }]}>dd-mm-yyyy</Text>
						</View>
						<View style={{ flexDirection: 'row', paddingLeft: 10 }}>
							<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.primary }]}>Time: </Text>
							<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm }]}>4am-12pm</Text>
						</View>
					</View>

					<Text numberOfLines={2} style={[Styles.itemText, { paddingBottom: 5, fontSize: Theme.sizes.sm }]}>
						Address: Akshya Nagar 1st Block 1st Cross, Rammurthy nagar, Bangalore-560016
					</Text>
				</View>
			</View> */}
			<View style={{borderWidth:1,borderColor:Theme.colors.primary,padding:10,marginBottom:10,borderRadius:5}}>
				<View style={{flexDirection:'row',justifyContent:'space-between'}}>
					<View style={{justifyContent:'center'}}>
							<View style={{ flexDirection: 'row' }}>
								<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm,color: Theme.colors.black,textTransform:'capitalize'}]}>Slot time: </Text>
								<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm,color: Theme.colors.primary  }]}>8pm- 10pm</Text>
							</View>
							<View style={{justifyContent:'center',paddingTop:2}}>
								<View style={{ flexDirection: 'row' }}>
									<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.black }]}>Price: </Text>
									<View style={{justifyContent:'center'}}>
										<Image source={require('../../../assets/images/rupee-indian.png')} style={{height:11,width:11}}  />
									</View>
									<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm,color: Theme.colors.primary }]}>500</Text>
								</View>
						</View>
					</View>
					{/* <View style={{justifyContent:'center'}}>
							<View style={{ flexDirection: 'row' }}>
								<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.primary }]}>Date: </Text>
								<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm }]}>dd-mm-yyyy</Text>
							</View>
					</View> */}
					<View style={{justifyContent:'center'}}>
							<View style={{ flexDirection: 'row' }}>
								<TouchableOpacity 
									onPress={()=>{props.navigation.navigate('CreateScheduleScreen')}}
									style={{paddingRight:15,justifyContent:'center'}}>
									<SimpleIcon name="pencil" color={Theme.colors.primary} size={18} />
								</TouchableOpacity>
								<TouchableOpacity 
									onPress={()=>{alert("hello")}}
									style={{paddingRight:15,justifyContent:'center'}}>
									<DisableIcon height={22} width={22} />
								</TouchableOpacity>
								<TouchableOpacity 
									onPress={()=>{alert("hello")}}
									style={{justifyContent:'center'}}>
									<CloseIcon height={22} width={22} />
								</TouchableOpacity>
							</View>
					</View>
					
				</View>
			</View>

		</TouchableOpacity>
	)

	const activeStyle = { borderColor: Theme.colors.black, backgroundColor: Theme.colors.black }
	const defaultStyle = { borderColor: Theme.colors.grey }
	const activeTextStyle = { color: Theme.colors.white }
	const defaultTextStyle = { color: Theme.colors.grey }


	return (


		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			// hTitle={'Schedule Appointments'}
			hTitle={'Schedule'}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={Theme.colors.primary}
			showHeaderLine={false}
			// customBackground={Theme.colors.white}
			customBackground={'#F8FBFF'}
			isBackBtn={false}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				props.navigation.goBack();

			}}
			backButton_animationRef={viewRef}
			showScrollView={true}
		>
			<View style={Styles.mainView}>
				<View style={{ height: 80, width: '100%', backgroundColor: Theme.colors.primary }}>
					<View style={{ width: '95%', alignSelf: 'center', backgroundColor: 'white', height: 100 }}></View>
				</View>

				<View style={{ position: 'absolute', top: 10, height: WindowData.customBody_height - 60, width: '95%', backgroundColor: Theme.colors.white, alignSelf: 'center', paddingHorizontal: 15, paddingVertical: 10 }}>
					{/* <Text
						style={{ fontFamily: Theme.FontFamily.medium, fontSize: Theme.sizes.h5, color: Theme.colors.black, paddingBottom: 10 }}
					>
						Your appointments
					</Text> */}

					<View style={{ flexDirection: 'row', paddingBottom: 10 }}>
						<TouchableOpacity
							onPress={() => { setSelectedIndex("icu") }}
							style={[selectedIndex == 'icu' ? activeStyle : defaultStyle, { borderWidth: 1, borderRadius: 6, flex: 1, justifyContent: 'center', alignItems: 'center', paddingVertical: 7, marginRight: 5, paddingHorizontal: 5 }]}>
							<View style={{ flexDirection: 'column', justifyContent: 'center' }}>
								<Text style={[selectedIndex == 'icu' ? activeTextStyle : defaultTextStyle, { fontSize: Theme.sizes.h7, letterSpacing: 0.6, textTransform: 'capitalize' }]}>icu/Emergency</Text>
							</View>
						</TouchableOpacity>
						{/* <TouchableOpacity
							onPress={() => { setSelectedIndex("emergency") }}
							style={[selectedIndex == 'emergency' ? activeStyle : defaultStyle, { borderWidth: 1, borderRadius: 6, flex: 1, justifyContent: 'center', alignItems: 'center', paddingVertical: 5, marginRight: 5, paddingHorizontal: 5 }]}>
							<View style={{ flexDirection: 'column', justifyContent: 'center' }}>
								<Text style={[selectedIndex == 'emergency' ? activeTextStyle : defaultTextStyle, { fontSize: Theme.sizes.h7, letterSpacing: 0.6, textTransform: 'capitalize' }]}>emergency</Text>
							</View>
						</TouchableOpacity> */}
						<TouchableOpacity
							onPress={() => { setSelectedIndex("consultation") }}
							style={[selectedIndex == 'consultation' ? activeStyle : defaultStyle, { borderWidth: 1, borderRadius: 6, flex: 1, justifyContent: 'center', alignItems: 'center', paddingVertical: 7, paddingHorizontal: 5 }]}>
							<View style={{ flexDirection: 'column', justifyContent: 'center' }}>
								<Text style={[selectedIndex == 'consultation' ? activeTextStyle : defaultTextStyle, { fontSize: Theme.sizes.h7, letterSpacing: 0.6, textTransform: 'capitalize' }]}>Offline Consultation</Text>
							</View>
						</TouchableOpacity>
						{/* <View style={{flex:2}}></View> */}
					</View>


					<ScrollView style={{ flex: 1 }}
						showsVerticalScrollIndicator={false}
						showsHorizontalScrollIndicator={false}
					>
						<View>
							<FlatList
								horizontal={false}
								data={selectedIndex == 'icu' ? icuDetails : 
									selectedIndex == 'emergency' ? emergencyDetails:
									selectedIndex == 'consultation' ? consultationDetails:icuDetails
								
								}
								renderItem={renderItem}
								// keyExtractor={(item, index) => item.id}
								keyExtractor={(item, index) => index}
								showsVerticalScrollIndicator={false}
								showsHorizontalScrollIndicator={false}
							/>
						</View>
					</ScrollView>

					<View style={{position:'absolute',bottom:'10%',left:'50%',justifyContent:'center'}}>
							<TouchableOpacity 
								onPress={()=>{props.navigation.navigate("CreateScheduleScreen")}}
								style={{padding:10,borderRadius:100, backgroundColor:Theme.colors.primary,justifyContent:'center',alignItems:'center'}}>
								{/* <FilterIcon height={27} width={27} /> */}
									<Icon name="add" size={25} color={Theme.colors.white} />
							</TouchableOpacity>
					</View>

					{Platform.OS == 'ios' ?
						<View style={{ paddingBottom: '8%' }}></View>
						: null}
				</View>


			</View>


		</ScreenLayout>


	)
}

export default ScheduleScreen
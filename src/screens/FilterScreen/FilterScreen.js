import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	Pressable
} from 'react-native';
import React, { useRef, useState } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../constants';
import { BlackButton, ScreenLayout } from '../../Components';
// import * as Animatable from 'react-native-animatable';
// import Permission from '../../assets/images/permission.svg';
import Style from './Style';
// import { Rating, AirbnbRating } from 'react-native-ratings';
// import { TextInput } from 'react-native';
import NotificationPermission from '../../assets/images/notification_permission.svg'
import MapPermission from '../../assets/images/map_permission.svg'
import { ScrollView } from 'react-native';
import { FlatList } from 'react-native';
import { Image } from 'react-native';
import DatePicker from 'react-native-date-picker';
import moment from "moment";

function FilterScreen(props) {
	const Styles = Style()
	const onPress = () => HelperFunctions.sampleFunction('its working fine');

	const selectedColor={ borderColor:Theme.colors.primary,color:Theme.colors.white,backgroundColor:Theme.colors.primary,letterSpacing:0.5,fontFamily:Theme.FontFamily.medium,fontWeight:'500'}
	const defaultColor={ borderColor:Theme.colors.background_shade1,color:Theme.colors.black,backgroundColor:Theme.colors.background_shade1,letterSpacing:0.5,fontFamily:Theme.FontFamily.normal,fontWeight:'400'}

	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */

	const [selectedIndex, setSelectedIndex] = useState(null);
	const [userValue, setuserValue] = useState(null);
	const [timeslotValue, settimeslotValue] = useState(null);

	const specialityDetails = [
		{ value: 'ICU'},{ value: 'Emergency'},{ value: 'Consultation'}
	]
	const userTypeDetails = [
		{ value: 'Doctors'},{ value: 'Nurse'}
	]
	const timeslotDetails = [
		{ value: '8am - 12pm'},
		{ value: '12pm - 4pm'},
		{ value: '4pm - 8pm'},
		{ value: '8pm - 12am'},
		{ value: '12am - 4am'},
		{ value: '4am - 8am'}
	]

	/* == Render menu item return function ==*/
	const renderItem = ({ item,index }) => (
		<TouchableOpacity style={{}} onPress={()=>{setSelectedIndex(index)}}>
			<View style={[selectedIndex == index ? selectedColor :defaultColor ,{minWidth:50, padding:5,paddingHorizontal:8,borderWidth:1,borderRadius:5,margin:5,justifyContent:'center',flexDirection:'column'}]}>
				<Text style={[selectedIndex == index  ? selectedColor :defaultColor,{fontSize:Theme.sizes.h7,textAlign:'center'}]}>{item?.value}</Text>
			</View>
		</TouchableOpacity>
	)

	/* == Render menu item return function ==*/
	const showUserLists = ({ item,index }) => (
		<TouchableOpacity style={{}} onPress={()=>{setuserValue(index)}}>
			<View style={[userValue == index ? selectedColor :defaultColor ,{minWidth:50, padding:5,paddingHorizontal:8,borderWidth:1,borderRadius:5,margin:5,justifyContent:'center',flexDirection:'column'}]}>
				<Text style={[userValue == index  ? selectedColor :defaultColor,{fontSize:Theme.sizes.h7,textAlign:'center'}]}>{item?.value}</Text>
			</View>
		</TouchableOpacity>
	)

	/* == Render menu item return function ==*/
	const showTimeSlot = ({ item,index }) => (
		<TouchableOpacity style={{}} onPress={()=>{settimeslotValue(index)}}>
			<View style={[timeslotValue == index ? selectedColor :defaultColor ,{minWidth:50, padding:5,paddingHorizontal:8,borderWidth:1,borderRadius:5,margin:5,justifyContent:'center',flexDirection:'column'}]}>
				<Text style={[timeslotValue == index  ? selectedColor :defaultColor,{fontSize:Theme.sizes.h7,textAlign:'center'}]}>{item?.value}</Text>
			</View>
		</TouchableOpacity>
	)

	const clearFilter = () => {
		setSelectedIndex(null);
		setuserValue(null);
		settimeslotValue(null);
		setTimeout(() => {
			props.navigation.goBack();
		}, 200);
	}

	const applyFilter = () => {
		setTimeout(() => {
			props.navigation.goBack();
		}, 200);
	}

	const [startDate, setStartDate] = useState(new Date())
	const [showStartDateModal, setShowStartDateModal] = useState(false)

	return (

		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={''}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={Theme.colors.primary}
			showHeaderLine={false}
			// customBackground={Theme.colors.white}
			customBackground={'#F8FBFF'}
			isBackBtn={true}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				props.navigation.goBack();

			}}
			backButton_animationRef={viewRef}
			showScrollView={true}
		>
			<View style={Styles.mainView}>
				<View style={{ height: 80, width: '100%', backgroundColor: Theme.colors.primary }}>
					<View style={{width:'90%',alignSelf:'center',backgroundColor:'white',height:100}}></View>
				</View>
				<View style={{ position: 'absolute', top: 0, height: WindowData.windowHeight-60, width: '90%', backgroundColor: Theme.colors.white, alignSelf: 'center', paddingHorizontal: 15, paddingVertical: 10 }}>
					<ScrollView style={{ flex: 1 }}>
						<View style={{paddingTop:10}}>
							<View>
								<Text style={{fontFamily:Theme.FontFamily.bold,fontWeight:'500',letterSpacing:0.5}}>Select Speciality	</Text>
							</View>
							<View style={{paddingTop:10}}>
								<FlatList
									// horizontal={true}
									data={specialityDetails}
									renderItem={renderItem}
									// keyExtractor={(item, index) => item.id}
									keyExtractor={(item, index) => index}
									showsVerticalScrollIndicator={false}
									showsHorizontalScrollIndicator={false}
									contentContainerStyle={{flexDirection: 'column'}}
									numColumns={3}
								/>
							</View>
						</View>

						<View style={{paddingTop:'10%'}}>
							<View>
								<Text style={{fontFamily:Theme.FontFamily.bold,fontWeight:'500',letterSpacing:0.5}}>Looking for the list of	</Text>
							</View>
							<View style={{paddingTop:10}}>
								<FlatList
									// horizontal={true}
									data={userTypeDetails}
									renderItem={showUserLists}
									// keyExtractor={(item, index) => item.id}
									keyExtractor={(item, index) => index}
									showsVerticalScrollIndicator={false}
									showsHorizontalScrollIndicator={false}
									contentContainerStyle={{flexDirection: 'column'}}
									numColumns={3}
								/>
							</View>
						</View>

						<View style={{paddingTop:'10%'}}>
							<View>
								{/* <Text style={{fontFamily:Theme.FontFamily.bold,fontWeight:'500',letterSpacing:0.5}}>Time Slots	</Text> */}
								<Text style={{fontFamily:Theme.FontFamily.bold,fontWeight:'500',letterSpacing:0.5}}>Choose Date	</Text>
							</View>
							<View style={{paddingTop:10}}>
								{/* <FlatList
									// horizontal={true}
									data={timeslotDetails}
									renderItem={showTimeSlot}
									// keyExtractor={(item, index) => item.id}
									keyExtractor={(item, index) => index}
									showsVerticalScrollIndicator={false}
									showsHorizontalScrollIndicator={false}
									contentContainerStyle={{flexDirection: 'column'}}
									numColumns={3}
								/> */}
								<TouchableOpacity style={{ flex: 1 }}
									onPress={() => {setShowStartDateModal(true)}}
								>
									<View style={{ minHeight: 45, borderWidth: 1, borderColor: Theme.colors.primary, borderRadius: 6, justifyContent: 'space-between',flexDirection:'row' }}>
										<View style={{justifyContent:'center',flex:1}}>
											<Text style={{ paddingLeft: 20, fontFamily: Theme.FontFamily.medium, letterSpacing: 0.5, fontSize: 12 }}>
												{startDate ? moment(startDate).format("DD-MM-YYYY") : 'Start Date'}
											</Text>
										</View>
										<View style={{justifyContent:'center',paddingRight:10}}>
											<Icon name="chevron-down-outline" size={18} color={Theme.colors.primary} />
										</View>
									</View>
									
								</TouchableOpacity>
								{/* Start Date picker */}
								<DatePicker
									modal open={showStartDateModal} date={startDate}
									onConfirm={(date) => {
										console.log("date : ",date)
										setShowStartDateModal(false)
										setStartDate(date);
									}}
									onCancel={() => { setShowStartDateModal(false) }}
									mode='date'
								/>
							</View>
						</View>

					</ScrollView>

					<View style={{height:150,flexDirection:'row',justifyContent:'space-evenly'}}>
						<View style={{flex:1}}>
							<BlackButton buttonText={'CLEAR'} onSubmitPress={() => { clearFilter() }} style={{ height: 44,backgroundColor:Theme.colors.background_shade1,borderColor:Theme.colors.background_shade1 }} textColor={Theme.colors.black} textCustomStyle={{ letterSpacing: 0.8 }} />
						</View>
						<View style={{flex:1}}>
							<BlackButton buttonText={'APPLY'} onSubmitPress={() => { applyFilter()}} style={{ height: 44 }} textColor={Theme.colors.primary} textCustomStyle={{ letterSpacing: 0.8 }} />
						</View>
					</View>

				</View>


			</View>


		</ScreenLayout>
	)
}

export default FilterScreen;
import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	ScrollView, Image
} from 'react-native';
import React, { useRef, useState } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../constants';

import Style from './Style';
import { BlackButton, ScreenLayout } from '../../Components';
// import RedCloseIcon from '../../assets/images/close_icon_red.svg'
import CongratulationIcon from '../../assets/images/congratulation_icon.svg';

// function CancelScreen({ navigation }) {
const CongratulationScreen = props => {

	const Styles = Style()
	const globalstyles = GlobalStyles();
	const onPress = () => HelperFunctions.sampleFunction('its working fine');

	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */
	const hospitalIconImage = require('../../assets/images/hospital_icon.png');
	
	const [selectedIndex, setSelectedIndex] = useState(null);

	const teamDetails = [
		{ value: '8am to 12pm' },
		{ value: '12pm to 4pm' },
		{ value: '4pm to 8pm' },
		{ value: '8pm to 12am' },
		{ value: '12am to 4am' },
		{ value: '4am to 8am' }
	]

	return (


		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={''}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={Theme.colors.primary}
			showHeaderLine={false}
			// customBackground={Theme.colors.white}
			customBackground={'#F8FBFF'}
			isBackBtn={false}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				navigation.goBack();

			}}
			backButton_animationRef={viewRef}
			showScrollView={true}
		>
			<View style={Styles.mainView}>
				<View style={{ height: 80, width: '100%', backgroundColor: Theme.colors.primary }}>
					<View style={{ width: '95%', alignSelf: 'center', backgroundColor: 'white', height: 100 }}></View>
				</View>

				<View style={{
					position: 'absolute', top: 0, height: WindowData.customBody_height, width: '95%', backgroundColor: Theme.colors.white, alignSelf: 'center'
					// paddingHorizontal: 15, paddingVertical: 10
				}}>
					
					<View style={{flex:1, flexDirection:'column',justifyContent:'center'}}>
						<View style={{justifyContent:'center',flexDirection:'row'}}>
							<CongratulationIcon height={130} width={130} />
						</View>
						<View style={{paddingTop:'10%'}}>
							<Text style={{paddingHorizontal:50,textAlign:'center',fontFamily:Theme.FontFamily.medium,fontSize:Theme.sizes.h1,color:Theme.colors.primary}}>
                                congratulation!!
                            </Text>
						</View>
						<View style={{paddingTop:'10%'}}>
							<Text style={{paddingHorizontal:10,textAlign:'center',fontFamily:Theme.FontFamily.bold,color:Theme.colors.black,letterSpacing:0.7}}>
							Your account has been created successfully. 
							</Text>
						</View>
						<View style={{paddingTop:'10%'}}>
							<BlackButton style={{height:45 }}  buttonText="Go to Login" textCustomStyle={{fontSize:Theme.sizes.sm}} onSubmitPress={()=>{props.navigation.replace('LoginScreen')}} />
						</View>

					</View>

				</View>


			</View>


		</ScreenLayout>


	)
}

export default CongratulationScreen;
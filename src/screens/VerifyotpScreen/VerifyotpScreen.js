import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	ScrollView, Image
} from 'react-native';
import React, { useRef, useState } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../constants';

import Style from './Style';
import { BlackButton, EditTextInputBox, ScreenLayout } from '../../Components';
// import RedCloseIcon from '../../assets/images/close_icon_red.svg'
import CancelIcon from '../../assets/images/cancelled_icon.svg';
// import ResetPasswordLogo from '../../../assets/images/ResetPassword.svg'
import KeyIcon from '../../assets/images/Key.svg'
import VerifyOtpIcon from '../../assets/images/verifyotp_screen.svg';

const VerifyotpScreen = props => {
	const Styles = Style()
	const globalstyles = GlobalStyles();
	const onPress = () => HelperFunctions.sampleFunction('its working fine');

	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */
	const hospitalIconImage = require('../../assets/images/hospital_icon.png');

	const [selectedIndex, setSelectedIndex] = useState(null);

	const teamDetails = [
		{ value: '8am to 12pm' },
		{ value: '12pm to 4pm' },
		{ value: '4pm to 8pm' },
		{ value: '8pm to 12am' },
		{ value: '12am to 4am' },
		{ value: '4am to 8am' }
	]

	return (


		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={''}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={'#F8FBFF'}
			// headerBackground={'red'}
			showHeaderLine={false}
			// customBackground={Theme.colors.white}
			customBackground={'#F8FBFF'}
			// customBackground={'red'}
			isBackBtn={true}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				props.navigation.goBack();

			}}
			backButton_animationRef={viewRef}
			showScrollView={false}
		>
			<View style={Styles.mainView}>
				<View style={{
					height: WindowData.customBody_height, width: '90%', backgroundColor: Theme.colors.white, alignSelf: 'center', paddingTop: '40%'
				}}>
					<View>
							<View>
								<Text style={{paddingHorizontal:30,textAlign:'center',fontFamily:Theme.FontFamily.medium,color:Theme.colors.black}}>
									A verification code has been {'\n'} sent to your number and email
								</Text>
							</View>
							<View style={{alignSelf:'center',paddingTop:10}}>
								<VerifyOtpIcon height={120} width={120} />
							</View>
							<View>
								<Text style={{paddingHorizontal:30,textAlign:'center',fontFamily:Theme.FontFamily.medium,color:Theme.colors.black,paddingTop:10,fontSize:Theme.sizes.h7}}>
								Enter the code here
								</Text>
							</View>

							<View style={{flexDirection:'row',justifyContent:'space-evenly',paddingHorizontal:'15%',paddingTop:20}}>
								<EditTextInputBox maxLength={1} keyboardType="numeric" style={{height:45,borderWith:1,borderColor:'#000',width:45}}/>
								<EditTextInputBox maxLength={1} keyboardType="numeric" style={{height:45,borderWith:1,borderColor:'#000',width:45}}/>
								<EditTextInputBox maxLength={1} keyboardType="numeric" style={{height:45,borderWith:1,borderColor:'#000',width:45}}/>
								<EditTextInputBox maxLength={1} keyboardType="numeric" style={{height:45,borderWith:1,borderColor:'#000',width:45}}/>
							</View>

							<View style={{marginTop:25}}>
									<BlackButton style={{height:45 }}  buttonText="NEXT" onSubmitPress={()=>{props.navigation.navigate('SetPasswordScreen')}} />
							</View>

							<View style={{flexDirection:'row',justifyContent:'center',paddingTop:20}}>
								<Text 
									style={{fontFamily:Theme.FontFamily.medium,fontSize:Theme.sizes.sm,color:Theme.colors.grey}}
								>
									Didn’t get code? 
								</Text>
								<TouchableOpacity style={{paddingLeft:5}}>
									<Text
									style={{fontFamily:Theme.FontFamily.medium,fontSize:Theme.sizes.sm,color:Theme.colors.black}}
									>
										Resend Code
									</Text>
								</TouchableOpacity>
							</View>

							<View style={{paddingTop:15}}>
								<Text 
								style={{textAlign:'center',fontFamily:Theme.FontFamily.bold,fontSize:Theme.sizes.sm,color:Theme.colors.primary}}
								>in 1:20 sec </Text>
							</View>

					</View>

				</View>

			</View>
			
		</ScreenLayout>


	)
}

export default VerifyotpScreen;
import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	ScrollView, Image, Platform
} from 'react-native';
import React, { useRef, useState } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../constants';
// import { Header, ScreenLayout } from '../../../Components';
import { FlatList } from 'react-native';
import Style from './Style';
import { BlackButton, ScreenLayout } from '../../Components';
import HospitalIcon from '../../assets/images/hospital_icon.svg'
import RedCloseIcon from '../../assets/images/close_icon_red.svg'
import {Calendar, LocaleConfig} from 'react-native-calendars';
import * as Animatable from 'react-native-animatable';

// function AppointmentDetailsScreen({ navigation }) {
const AppointmentDetailsScreen = props => {
	const Styles = Style()
	const globalstyles = GlobalStyles();
	const onPress = () => HelperFunctions.sampleFunction('its working fine');

	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */
	const hospitalIconImage = require('../../assets/images/hospital_icon.png');
	
	const [selectedIndex, setSelectedIndex] = useState(null);
	
	const teamDetails = [
		{ value: '8am to 12pm'},
		// { value: '12pm to 4pm'},
		// { value: '4pm to 8pm'},
		// { value: '8pm to 12am'},
		// { value: '12am to 4am'},
		// { value: '4am to 8am'}
	]
	const selectedColor={ borderColor:'#0B8B18',color:'#0B8B18'}
	// const defaultColor={ borderColor:Theme.colors.grey,color:Theme.colors.grey}
	const defaultColor={ borderColor:'#0B8B18',color:'#0B8B18'}

	/* == Render menu item return function ==*/
	const renderItem = ({ item,index }) => (
		<TouchableOpacity style={{}} onPress={()=>{setSelectedIndex(index)}}>
			<View style={[selectedIndex == index ? selectedColor :defaultColor ,{padding:5,paddingHorizontal:8,borderWidth:1,borderRadius:8,margin:5,marginLeft:0,justifyContent:'center',flexDirection:'column'}]}>
				<Text style={[selectedIndex == index  ? selectedColor :defaultColor,{fontSize:Theme.sizes.h7,textAlign:'center'}]}>{item?.value}</Text>
			</View>
		</TouchableOpacity>
	)


	const [selected, setSelected] = useState('');
	return (


		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={'Hospital Details'}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={Theme.colors.primary}
			showHeaderLine={false}
			headerTextStyle={{color:Theme.colors.black}}
			// customBackground={Theme.colors.white}
			customBackground={'#F8FBFF'}
			isBackBtn={true}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				props.navigation.goBack();

			}}
			backButton_animationRef={viewRef}
			showScrollView={true}
		>
			<View style={Styles.mainView}>
				<View style={{ height: 80, width: '100%', backgroundColor: Theme.colors.primary }}>
					<View style={{ width: '95%', alignSelf: 'center', backgroundColor: 'white', height: 100 }}></View>
				</View>

				<View style={{ position: 'absolute', top: 0, height: WindowData.customBody_height, width: '95%', backgroundColor: Theme.colors.white, alignSelf: 'center'
				// paddingHorizontal: 15, paddingVertical: 10
				}}>
					<View 
						style={{width:'100%',
							padding:1,borderWidth:0.5,borderColor:Theme.colors.grey,

						}}
					>
						<View style={{flexDirection:'row',paddingVertical:10,paddingHorizontal:10}}>
							<View style={{height:80,width:80,borderRadius:5}}>
								<Image source={require('../../assets/images/demo_profile_one.jpeg')} style={{height:79,width:79,borderRadius:5,resizeMode:'cover',borderColor:Theme.colors.primary,borderWidth:1}} />
							</View>
							<View style={{flex:1,paddingLeft:10}}>
								<Text style={[Styles.itemText, { paddingBottom: 5, fontFamily:Theme.FontFamily.bold, fontSize: Theme.sizes.h6,letterSpacing:0.7 }]}>Doctor's Name</Text>
								
								<View style={{flexDirection:'row'}}>
									<Text style={[Styles.itemText, { paddingBottom: 5, fontWeight: '600', fontSize: Theme.sizes.sm,color:Theme.colors.primary }]}>Description: </Text>
									<Text style={[Styles.itemText, { paddingBottom: 5, fontWeight: '600', fontSize: Theme.sizes.sm,color:Theme.colors.grey,letterSpacing:0.5 }]}>{props.route.params.item?.type}</Text>
								</View>
								<View style={{flexDirection:'row'}}>
									<Text style={[Styles.itemText, { paddingBottom: 5, fontWeight: '600', fontSize: Theme.sizes.sm,color:Theme.colors.primary }]}>Speciality: </Text>
									<Text style={[Styles.itemText, { paddingBottom: 5, fontWeight: '600', fontSize: Theme.sizes.sm,color:Theme.colors.black,letterSpacing:0.5 }]}>Orthopedic</Text>
								</View>
								<View style={{flexDirection:'row'}}>
									<Text style={[Styles.itemText, { paddingBottom: 5, fontWeight: '600', fontSize: Theme.sizes.sm,color:Theme.colors.primary }]}>Staff: </Text>
									<Text style={[Styles.itemText, { paddingBottom: 5, fontWeight: '600', fontSize: Theme.sizes.sm,color:Theme.colors.black,letterSpacing:0.5 }]}>Doctor</Text>
								</View>

								{/* <View style={{flexDirection:'row',paddingRight:10}}>
									<Image source={hospitalIconImage} style={{height:15,width:15,resizeMode:'cover',marginTop:2}} />
									<Text numberOfLines={2} style={[Styles.itemText, { paddingBottom: 5, fontWeight: '600', fontSize: Theme.sizes.sm,color:Theme.colors.grey,paddingLeft:5 }]}>
										Address: Akshya Nagar 1st Block 1st Cross, Rammurthy nagar, Bangalore-560016
									</Text>
								</View> */}
								{props.route.params?.item?.type == 'Consultation' ?
									<View style={{flexDirection:'row',justifyContent:'flex-start'}}>
										<Animatable.View
											animation={'pulse'}
											easing="ease-out"
											iterationCount="infinite"
											style={{ borderWidth: 1, borderRadius: 5, justifyContent: 'center', flexDirection: 'column', borderColor: Theme.colors.black,paddingHorizontal:5 }}>
											<Text style={[Styles.itemText, { fontSize: Theme.sizes.sm, color: Theme.colors.black, paddingHorizontal: 2, paddingVertical: 3, textAlign: 'center' }]}>{props.route.params.item?.mode} CONSULTATION</Text>
										</Animatable.View>

										{/* {props.route.params.item?.mode == 'ONLINE' ?
											<View style={{flexDirection:'row',paddingLeft:20}}>
												<Animatable.View
													animation={'pulse'}
													easing="ease-out"
													iterationCount="infinite"
													style={{ justifyContent: 'center', flexDirection: 'column',}}>
													<Icon name="call" size={20} />
												</Animatable.View>
												<Animatable.View
													animation={'pulse'}
													easing="ease-out"
													iterationCount="infinite"
													style={{ justifyContent: 'center', flexDirection: 'column',paddingLeft:20}}>
													<Icon name="videocam" size={20} />
												</Animatable.View>
										</View>
										:null} */}
										

									</View>
									: null
								}

							</View>
							{/* <View style={{justifyContent:'flex-end',flexDirection:'row'}}>
								<Icon name="heart-outline" size={25} onPress={()=>{alert("heart clicked")}} />
							</View> */}

						</View>


					</View>

					<ScrollView style={{ flex: 1 }}
						showsVerticalScrollIndicator={false}
						showsHorizontalScrollIndicator={false}
					>
						<View style={{flexDirection:'row',justifyContent:'center',marginTop:10,marginRight:10}}>
							<TouchableOpacity  onPress={()=>{props.navigation.navigate('RequestlistScreen')}}>
								<View style={{paddingVertical:7,paddingHorizontal:10,borderWidth:1,borderColor:Theme.colors.black,borderRadius:6,fontFamily:Theme.FontFamily.medium,letterSpacing:0.5,color:Theme.colors.black}}>
									<Text style={{fontFamily:Theme.FontFamily.medium,color:Theme.colors.black}}>View Request list</Text>
								</View>
							</TouchableOpacity>
						</View>


						<View style={{paddingBottom:10,borderBottomWidth:0.8,borderBottomColor:Theme.colors.grey,paddingHorizontal:10}}>
							<View style={{paddingTop:20}}>
								<Text style={{fontFamily:Theme.FontFamily.medium,fontWeight:'600',color:Theme.colors.black}}>Scheduled Date</Text>
							</View>
							<View style={{borderWidth:1,borderRadius:10,marginVertical:10,padding:5,borderColor:Theme.colors.primary}}>
								<Calendar
									onDayPress={day => {
										setSelected(day.dateString);
									}}
									markedDates={{
										[selected]: {selected: true, disableTouchEvent: true, selectedDotColor: 'orange'}
									}}
								/>
							</View>
						</View>

						<View style={{paddingBottom:10,paddingHorizontal:10}}>
							<View style={{paddingTop:20,paddingBottom:5}}>
								<Text style={{fontFamily:Theme.FontFamily.medium,fontWeight:'600',color:Theme.colors.black}}>Scheduled Time</Text>
							</View>
							<View style={{flexDirection:'row',flexWrap:'wrap'}}>
								<FlatList
									// horizontal={true}
									data={teamDetails}
									renderItem={renderItem}
									// keyExtractor={(item, index) => item.id}
									keyExtractor={(item, index) => index}
									showsVerticalScrollIndicator={false}
									showsHorizontalScrollIndicator={false}
									contentContainerStyle={{flexDirection: 'column'}}
									numColumns={3}
								/>
							</View>
						</View>
						
						{props.route.params?.item?.mode == 'ONLINE'?
							<View style={{paddingTop:10}}>
								<BlackButton buttonText={'START CONSULTATION'} onSubmitPress={() => { }} style={{ height: 44 }} textColor={Theme.colors.primary} textCustomStyle={{ letterSpacing: 0.8 }} />
							</View>
							:null
						}

						<View style={{paddingBottom:10,paddingTop:15}}>
							<TouchableOpacity 
								onPress={()=>{props.navigation.navigate('CancelScreen')}}
								style={{height:44, width: '78%', borderRadius: 8,borderWidth:1,borderColor:'#E6E6E6',
								alignSelf: 'center',backgroundColor:'#E6E6E6',flexDirection:'row',justifyContent:'center'}}
							>
								<View style={{justifyContent:'center',flexDirection:'column'}}>
									<RedCloseIcon height={20} width={20} />
								</View>
								<View style={{justifyContent:'center',flexDirection:'column',paddingLeft:10}}>
									<Text style={{color:'#E11712',fontWeight:'600',fontFamily:Theme.FontFamily.medium,letterSpacing:0.5}}>CANCEL BOOKING</Text>
								</View>
										
							</TouchableOpacity>
						</View>


					</ScrollView>
					{Platform.OS == 'ios'?
						<View style={{paddingBottom:'8%'}}></View>
					:null}
				</View>


			</View>


		</ScreenLayout>


	)
}

export default AppointmentDetailsScreen;
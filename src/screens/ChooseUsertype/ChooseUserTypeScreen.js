import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	Pressable
} from 'react-native';
import React, { useRef, useState } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../constants';
import { BlackButton, DropdownPicker, EditTextInputBox, ScreenLayout } from '../../Components';
// import * as Animatable from 'react-native-animatable';
// import Permission from '../../assets/images/permission.svg';
import Style from './Style';
// import { Rating, AirbnbRating } from 'react-native-ratings';
// import { TextInput } from 'react-native';
import NotificationPermission from '../../assets/images/notification_permission.svg';
import MapPermission from '../../assets/images/map_permission.svg';
import HospitalIcon from '../../assets/images/Hospital_img.svg';
import { ScrollView } from 'react-native';
import { FlatList } from 'react-native';
import { Image } from 'react-native';
import DoctorIcon from '../../assets/images/doctor_icon.svg'
import NurseIcon from '../../assets/images/nurse_icon.svg'
import ModalBottom from '../../Components/ModalBottom/ModalBottom';

function HospitalInfoScreen(props) {
	const Styles = Style()
	const onPress = () => HelperFunctions.sampleFunction('its working fine');

	// const [locationToggleValue, setLocationToggleValue] = useState(false);
	// const [notifiationToggleValue, setNotifiationToggleValue] = useState(false);
	const [selectedType,setSelectedType]=useState(null)


	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */

	const [documentBottomVisible,setDocumentBottomVisible]=useState(false)
	const [selectedDocumentType,setSelectedDocumentType]=useState("")
	const closeDocumentType=()=>{
		setDocumentBottomVisible(false)
	}



	return (

		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={'Owner Information'}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={Theme.colors.white}
			showHeaderLine={false}
			headerlineBorder={'black'}
			headerlineBackground={'black'}
			customBackground={'#F8FBFF'}
			// customBackground={Theme.colors.white}
			isBackBtn={true}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				// props.navigation.goBack();
				props.navigation.replace('LoginScreen')

			}}
			backButton_animationRef={viewRef}
			showScrollView={false}
		>
			<View style={{paddingHorizontal:0,backgroundColor:'white',
				shadowColor: "#000",
				shadowOffset: {
					width: 0,
					height: 1,
				},
				shadowOpacity: 0.18,
				shadowRadius: 1.00,
				elevation: 1,
			}}>
				<View style={{alignSelf:'center',justifyContent:'center',paddingVertical:'15%'}}>
					<HospitalIcon height={200}  />
				</View>
			</View>
			<View>
				<EditTextInputBox placeHolderText="Hospital name*" style={{marginTop:'9%',height:50}}/>
				
				<DropdownPicker
					selectedValue={'Select country'} 
					setDocumentBottomVisible={()=>{
						setDocumentBottomVisible(true)
						setSelectedDocumentType("Countries")
					}} 
					style={{marginTop:'7%',height:50}}
					
				/>
				<ModalBottom modalCloseAction={()=>{closeDocumentType()}} 
					modalHeaderText={selectedDocumentType} 
					modalVisible={documentBottomVisible}>
					<View style={{paddingTop:40}}>
						<Text style={{fontSize:Theme.sizes.h5,lineHeight:25,textAlign:'center',fontFamily:Theme.FontFamily.medium,color:'black'}}>Data should be dynamic, and We will implement in the time of development phase</Text>
					</View>
				</ModalBottom>

				<BlackButton style={{ marginTop: '13%',height:50 }} buttonText="NEXT" onSubmitPress={()=>{
					props.navigation.navigate('PersonalInformation')
					}} />

			</View>
			<View style={{ flexDirection: 'row', paddingTop:10, alignSelf: 'center', alignItems: 'center' }}>
          <View style={{justifyContent:'center'}}>
						<Text style={Styles.newUserText}>Already have account? </Text>
					</View>
          <TouchableOpacity
            style={Styles.createAccountStyles}
            onPress={()=>{props.navigation.replace('LoginScreen')}}
          >
            <Text
              style={Styles.createAccountTextStyles}
            >Login</Text>
          </TouchableOpacity>
        </View>


		</ScreenLayout>
	)
}

export default HospitalInfoScreen;